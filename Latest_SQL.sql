# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.7.10-log
# Server OS:                    Win64
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2017-04-09 11:13:21
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping database structure for projectsdb
DROP DATABASE IF EXISTS `projectsdb`;
CREATE DATABASE IF NOT EXISTS `projectsdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `projectsdb`;


# Dumping structure for table projectsdb.hmm_branch
DROP TABLE IF EXISTS `hmm_branch`;
CREATE TABLE IF NOT EXISTS `hmm_branch` (
  `branch_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(10) NOT NULL DEFAULT '0',
  `branch_name` varchar(50) NOT NULL DEFAULT '',
  `branch_address1` varchar(200) NOT NULL DEFAULT '',
  `branch_latitude` varchar(100) NOT NULL DEFAULT '',
  `branch_longitude` varchar(100) NOT NULL DEFAULT '',
  `branch_address2` varchar(200) NOT NULL DEFAULT '',
  `branch_city` varchar(20) NOT NULL DEFAULT '',
  `branch_state` varchar(20) NOT NULL DEFAULT '',
  `branch_zip` varchar(10) NOT NULL DEFAULT '',
  `branch_is_active` varchar(1) NOT NULL DEFAULT '',
  `branch_start_time` varchar(5) NOT NULL DEFAULT '',
  `branch_end_time` varchar(5) NOT NULL DEFAULT '',
  `branch_is_closed` varchar(3) NOT NULL DEFAULT '',
  `branch_phone` varchar(13) NOT NULL DEFAULT '',
  `branch_map` varchar(200) NOT NULL DEFAULT '',
  `branch_currency` varchar(3) DEFAULT NULL,
  `branch_kit_desc` varchar(1000) DEFAULT NULL,
  `branch_kit_close` varchar(3) DEFAULT NULL,
  `branch_time` varchar(1000) DEFAULT NULL,
  `branch_message` varchar(1000) DEFAULT NULL,
  `branch_email` varchar(100) DEFAULT NULL,
  `branch_fb` varchar(500) DEFAULT NULL,
  `branch_tax` float DEFAULT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.hmm_branch: ~2 rows (approximately)
/*!40000 ALTER TABLE `hmm_branch` DISABLE KEYS */;
INSERT INTO `hmm_branch` (`branch_id`, `company_id`, `branch_name`, `branch_address1`, `branch_latitude`, `branch_longitude`, `branch_address2`, `branch_city`, `branch_state`, `branch_zip`, `branch_is_active`, `branch_start_time`, `branch_end_time`, `branch_is_closed`, `branch_phone`, `branch_map`, `branch_currency`, `branch_kit_desc`, `branch_kit_close`, `branch_time`, `branch_message`, `branch_email`, `branch_fb`, `branch_tax`) VALUES
	(1, 1, 'Allentown-PA', '1894 Catasauqua Rd', '40.640384', '-75.429137', '', 'Allentown', 'PA', '18109', 'Y', '11:30', '10:00', 'NO', '610-419-8700', '', '$', 'Kitchen Closed for Christmas', 'NO', '[{"sunday":[{"open":"12:00","close":"01:00"},{"open":"12:00","close":"01:00"}]},{"Monday":[{"open":"12:00","close":"01:00"},{"open":"12:00","close":"01:00"}]}, {"Tuesday":[{"open":"12:00","close":"01:00"},{"open":"12:00","close":"01:00"}]},{"Wednessday":[{"open":"12:00","close":"01:00"},{"open":"12:00","close":"01:00"}]}, {"Thursday":[{"open":"12:00","close":"01:00"},{"open":"12:00","close":"01:00"}]},{"Saturday":[{"open":"12:00","close":"01:00"},{"open":"12:00","close":"01:00"}]}]', 'TEst1234', 'briyanicity@bc.com', 'http://yahoo.com', 2.5),
	(2, 1, 'Norristown-PA', '3140 Ridge Pike', '40.6084', '75.4902', '', 'Norristown', 'PA', '19403', 'Y', '11:30', '10:00', 'NO', '610-539-8362', '', '$', NULL, 'YES', '[{"sunday":[{"open":"12:00","close":"01:00"},{"open":"12:00","close":"01:00"}]},{"Monday":[{"open":"12:00","close":"01:00"},{"open":"12:00","close":"01:00"}]}, {"Tuesday":[{"open":"12:00","close":"01:00"},{"open":"12:00","close":"01:00"}]},{"Wednessday":[{"open":"12:00","close":"01:00"},{"open":"12:00","close":"01:00"}]}, {"Thursday":[{"open":"12:00","close":"01:00"},{"open":"12:00","close":"01:00"}]},{"Saturday":[{"open":"12:00","close":"01:00"},{"open":"12:00","close":"01:00"}]}]', 'test123456756856uhfchb', 'briyanicity@bc.com', 'http://yahoo.com', 3.6);
/*!40000 ALTER TABLE `hmm_branch` ENABLE KEYS */;


# Dumping structure for table projectsdb.hmm_cat
DROP TABLE IF EXISTS `hmm_cat`;
CREATE TABLE IF NOT EXISTS `hmm_cat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `cat_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.hmm_cat: ~5 rows (approximately)
/*!40000 ALTER TABLE `hmm_cat` DISABLE KEYS */;
INSERT INTO `hmm_cat` (`id`, `name`, `cat_order`) VALUES
	(1, 'FOOD', 1),
	(2, 'DESSERT', 2),
	(3, 'BEVARAGES', 3),
	(4, 'CATERING SERVICES', 4),
	(5, 'ON SITE DOSA PARTIES', 5);
/*!40000 ALTER TABLE `hmm_cat` ENABLE KEYS */;


# Dumping structure for table projectsdb.hmm_company
DROP TABLE IF EXISTS `hmm_company`;
CREATE TABLE IF NOT EXISTS `hmm_company` (
  `company_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) NOT NULL DEFAULT '',
  `company_slogan` varchar(200) DEFAULT NULL,
  `company_url` varchar(200) DEFAULT NULL,
  `company_image` varchar(500) DEFAULT NULL,
  `company_logo` varchar(500) DEFAULT NULL,
  `company_theme` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.hmm_company: ~1 rows (approximately)
/*!40000 ALTER TABLE `hmm_company` DISABLE KEYS */;
INSERT INTO `hmm_company` (`company_id`, `company_name`, `company_slogan`, `company_url`, `company_image`, `company_logo`, `company_theme`) VALUES
	(1, 'BriyaniCity', 'Taste the Best', 'www.briyaniCity.com', NULL, 'bc_logo.png', 'red');
/*!40000 ALTER TABLE `hmm_company` ENABLE KEYS */;


# Dumping structure for table projectsdb.hmm_order
DROP TABLE IF EXISTS `hmm_order`;
CREATE TABLE IF NOT EXISTS `hmm_order` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_by` varchar(100) NOT NULL DEFAULT '',
  `order_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `order_cook` varchar(50) NOT NULL DEFAULT '',
  `order_price` int(10) DEFAULT '-1',
  `order_description` varchar(1500) DEFAULT '',
  `order_status` varchar(10) NOT NULL DEFAULT '0',
  `order_user` varchar(50) NOT NULL DEFAULT 'NO',
  `order_email` varchar(50) NOT NULL DEFAULT 'NO',
  `order_branch_id` int(10) DEFAULT NULL,
  `order_tax` varchar(500) DEFAULT NULL,
  `order_tips` varchar(10) DEFAULT NULL,
  `order_type` varchar(50) DEFAULT NULL,
  `order_phone` varchar(50) DEFAULT NULL,
  `order_paid` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.hmm_order: ~30 rows (approximately)
/*!40000 ALTER TABLE `hmm_order` DISABLE KEYS */;
INSERT INTO `hmm_order` (`order_id`, `order_by`, `order_date`, `order_cook`, `order_price`, `order_description`, `order_status`, `order_user`, `order_email`, `order_branch_id`, `order_tax`, `order_tips`, `order_type`, `order_phone`, `order_paid`) VALUES
	(42, '', '2017-02-28 10:50:26', '', 18, '', 'Cancelled', 'apple', 'pnbalu@gmail.com', 1, NULL, '12', 'Internet', '99', 0),
	(43, '', '2017-02-28 12:13:13', '', 22, '', 'Cancelled', 'tin', 'pnbalu@gmail.com', 1, NULL, '12', 'Internet', '99', 0),
	(44, '', '2017-02-28 12:18:52', '', 13, '', 'Ready', 'sak', 'pnbalu@gmail.com', 1, NULL, '10', 'Internet', '555', 0),
	(45, '', '2017-02-28 12:20:52', '', 117, '', 'Ready', 'Balusamy Nachiappan', 'pnbalu@gmail.com', 1, NULL, '2', 'To Go', '44', 0),
	(46, '', '2017-02-28 12:20:55', '', 117, '', 'InProgress', 'Balusamy Nachiappan', 'pnbalu@gmail.com', 1, NULL, '2', 'To Go', '44', 0),
	(47, '', '2017-02-28 12:26:36', '', 17, '', 'Ordered', 'sak', 'pnbalu@gmail.com', 1, NULL, '12', 'Internet', '666', 0),
	(54, '', '2017-03-02 06:49:00', '', 12, '', 'Cancelled', 'hi', 'pnbalu@gmail.com', 1, NULL, '10', 'Internet', '11', 0),
	(55, '', '2017-03-04 16:30:42', '', 15, '', 'Ordered', 'sah', 'pnbalu@gmail.com', 1, NULL, '5', 'Internet', '11', 0),
	(56, '', '2017-03-04 16:42:12', '', 118, '', 'Ordered', 'Balusamy Nachiappan', 'pnbalu@gmail.com', 1, NULL, '10', 'To Go', '12345', 0),
	(57, '', '2017-03-04 16:42:15', '', 118, '', 'Ordered', 'Balusamy Nachiappan', 'pnbalu@gmail.com', 1, NULL, '10', 'To Go', '12345', 0),
	(58, '', '2017-03-04 16:42:21', '', 118, '', 'InProgress', 'Balusamy Nachiappan', 'pnbalu@gmail.com', 1, NULL, '10', 'To Go', '12345', 0),
	(59, '', '2017-03-04 16:43:01', '', 119, '', 'Delivered', 'Balusamy Nachiappan', 'pnbalu@gmail.com', 1, NULL, '0', 'To Go', '54321', 0),
	(60, '', '2017-03-04 16:43:51', '', 119, '', 'Delivered', 'Balusamy Nachiappan', 'pnbalu@gmail.com', 1, NULL, '0', 'To Go', '54321', 0),
	(61, '', '2017-03-04 17:00:44', '', 18, '', 'Hold', 'kok', 'pnbalu@gmail.com', 1, NULL, '10', 'Internet', '445566', 0),
	(62, '', '2017-03-11 09:20:54', '', 24, '', 'Ordered', '222', 'pnbalu@gmail.com', 2, NULL, '23', 'Internet', '222', 0),
	(63, '', '2017-03-11 11:11:22', '', 25, '', 'Ordered', '2', 'pnbalu@gmail.com', 2, NULL, '2', 'Internet', '2', 0),
	(64, '', '2017-03-11 12:17:45', '', 7, '', 'Ordered', '3', 'pnbalu@gmail.com', 2, NULL, '3', 'Internet', '3', 0),
	(65, '', '2017-03-14 21:08:59', '', 25, '', 'Ordered', 'we', 'pnbalu@gmail.com', 2, NULL, '23', 'Internet', 'wew', 0),
	(66, '', '2017-03-14 21:19:46', '', 25, '', 'Ordered', 'we', 'pnbalu@gmail.com', 2, NULL, '23', 'Internet', 'wew', 0),
	(67, '', '2017-03-14 21:21:45', '', 8, '', 'Ordered', '3434', 'pnbalu@gmail.com', 2, NULL, '34', 'Internet', '34', 0),
	(68, '', '2017-03-14 21:23:59', '', 15, '', 'Ordered', '3434', 'pnbalu@gmail.com', 2, NULL, '34', 'Internet', '34', 0),
	(69, '', '2017-03-14 21:29:02', '', 15, '', 'Ordered', '3434', 'pnbalu@gmail.com', 2, NULL, '12', 'Internet', '34', 0),
	(70, '', '2017-03-14 21:33:41', '', 16, '', 'Ordered', '54', 'pnbalu@gmail.com', 2, NULL, '4', 'Internet', '5', 0),
	(71, '', '2017-03-14 21:43:22', '', 17, '', 'Ordered', '2345', 'pnbalu@gmail.com', 2, NULL, '23456', 'Internet', '3456', 0),
	(72, '', '2017-03-14 21:47:11', '', 7, '', 'Ordered', '2', 'pnbalu@gmail.com', 1, NULL, '5', 'Internet', '3', 0),
	(73, '', '2017-03-14 21:53:08', '', 8, '', 'Ordered', '2', 'pnbalu@gmail.com', 1, NULL, '2', 'Internet', '2', 0),
	(74, '', '2017-03-14 22:12:29', '', 7, '', 'Ordered', '2', 'pnbalu@gmail.com', 1, NULL, '2', 'Internet', '3', 0),
	(75, '', '2017-04-01 12:23:56', '', 16, '', 'Ordered', 'ssssssss', 'pnbalu@gmail.com', 1, NULL, '23', 'Internet', 'sssssss', 1),
	(76, '', '2017-04-01 12:38:21', '', 25, '', 'Ordered', 'qqqqqqqqq', 'pnbalu@gmail.com', 1, NULL, '23', 'Internet', '345345', 1),
	(77, '', '2017-04-08 17:28:18', '', -1, '', '0', 'NO', 'pnbalu@gmail.com', NULL, NULL, NULL, NULL, NULL, 0);
/*!40000 ALTER TABLE `hmm_order` ENABLE KEYS */;


# Dumping structure for table projectsdb.hmm_order_details
DROP TABLE IF EXISTS `hmm_order_details`;
CREATE TABLE IF NOT EXISTS `hmm_order_details` (
  `orderd_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderd_status` varchar(50) NOT NULL DEFAULT '0',
  `orderd_order_id` int(10) NOT NULL DEFAULT '0',
  `orderd_name` varchar(50) NOT NULL DEFAULT '',
  `orderd_price` int(10) DEFAULT '-1',
  `orderd_contents` varchar(500) DEFAULT NULL,
  `orderd_branch` int(10) DEFAULT NULL,
  `orderd_description` varchar(1000) DEFAULT NULL,
  `orderd_customized` int(1) DEFAULT NULL,
  PRIMARY KEY (`orderd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.hmm_order_details: ~108 rows (approximately)
/*!40000 ALTER TABLE `hmm_order_details` DISABLE KEYS */;
INSERT INTO `hmm_order_details` (`orderd_id`, `orderd_status`, `orderd_order_id`, `orderd_name`, `orderd_price`, `orderd_contents`, `orderd_branch`, `orderd_description`, `orderd_customized`) VALUES
	(16, '0', 15, 'Rava Roast', 5, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, 1),
	(17, 'Cancelled', 15, 'Paper Roast', 19, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, 0),
	(18, 'Cancelled', 15, 'Plain Roast', 8, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, 1),
	(19, 'Cancelled', 15, 'Black Tea', 2, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, 0),
	(20, 'Cancelled', 15, 'Butter Idli - 2 Pcs', 9, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, 0),
	(21, '0', 16, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, 0),
	(22, '0', 16, 'Chilli Bajji - 5 Pcs', 8, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, 0),
	(23, '0', 16, 'Pakodas - 12 oz', 7, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, 0),
	(24, '0', 17, 'Chilli Bajji - 5 Pcs', 8, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(25, '0', 17, 'Pakodas - 12 oz', 7, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(26, '0', 17, 'Plain Roast', 8, '{}', NULL, NULL, NULL),
	(27, '0', 17, 'Cheese Dosa', 5, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(28, '0', 17, 'Rava Roast', 5, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(29, '0', 17, 'Samosas - 5 Pcs', 7, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(30, '0', 18, 'Coffee', 3, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(31, '0', 18, 'Masala Tea', 3, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(32, '0', 18, 'Black Tea', 2, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(33, '0', 19, 'Coffee', 3, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(34, '0', 19, 'Masala Tea', 3, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(35, '0', 19, 'Black Tea', 2, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(36, '0', 20, 'Coffee', 3, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(37, '0', 20, 'Masala Tea', 3, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(38, '0', 20, 'Black Tea', 2, '{"spice":3,"salt":1,"sour":1}', NULL, NULL, NULL),
	(39, '0', 25, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, 'Coated in a Manchurian sauce made from scratch (ginger and garlic, soy sauce, chili sauces, and vinegar)', 1),
	(40, '0', 26, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, 'Coated in a Manchurian sauce made from scratch (ginger and garlic, soy sauce, chili sauces, and vinegar)', 1),
	(41, '0', 26, 'Chilli Bajji - 5 Pcs', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'Chili and potato stuffing coated with batter and fried, served hot with tomato sauce or occasionally with mint and tamarind chutney.', 0),
	(42, '0', 26, 'Pakodas - 12 oz', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'Crispy fried onions or Spinach dipped in a chickpea and rice flour batter. Just like amma used to make.', 1),
	(43, '0', 27, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, 'Coated in a Manchurian sauce made from scratch (ginger and garlic, soy sauce, chili sauces, and vinegar)', 1),
	(44, '0', 27, 'Chilli Bajji - 5 Pcs', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'Chili and potato stuffing coated with batter and fried, served hot with tomato sauce or occasionally with mint and tamarind chutney.', 0),
	(45, '0', 27, 'Pakodas - 12 oz', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'Crispy fried onions or Spinach dipped in a chickpea and rice flour batter. Just like amma used to make.', 1),
	(46, '0', 28, 'Cheese Dosa', 5, '{"spice":3,"salt":1,"sour":1}', NULL, 'dddddddddddddd', 1),
	(47, '0', 29, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(48, '0', 29, 'Samosas - 5 Pcs', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(49, '0', 30, 'undefined', 8, '', NULL, '', 0),
	(50, '0', 30, 'undefined', 7, '', NULL, '', 0),
	(51, '0', 31, 'undefined', 8, '', NULL, '', 0),
	(52, '0', 31, 'undefined', 7, '', NULL, '', 0),
	(53, '0', 32, 'undefined', 8, '', NULL, '', 0),
	(54, '0', 32, 'undefined', 7, '', NULL, '', 0),
	(55, '0', 33, 'undefined', 8, '', NULL, '', 0),
	(56, '0', 33, 'undefined', 7, '', NULL, '', 0),
	(57, '0', 34, 'undefined', 8, '', NULL, '', 0),
	(58, '0', 34, 'undefined', 7, '', NULL, '', 0),
	(59, '0', 35, 'Samosas - 5 Pcs', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(60, '0', 35, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(61, '0', 36, 'Pakodas - 12 oz', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(62, '0', 37, 'undefined', 9, '', NULL, '', 0),
	(63, '0', 38, 'undefined', 9, '', NULL, '', 1),
	(64, '0', 39, 'undefined', 9, '', NULL, '', 1),
	(65, '0', 40, 'undefined', 9, '', NULL, '', 1),
	(66, '0', 41, 'undefined', 9, '', NULL, '', 1),
	(67, 'Cancelled', 42, 'Chilli Bajji - 5 Pcs', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(68, '0', 42, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(69, '0', 43, 'Coffee', 3, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(70, '0', 43, 'Paper Roast', 19, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(71, '0', 44, 'Chilli Bajji - 5 Pcs', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(72, '0', 44, 'Rava Roast', 5, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(73, 'Cancelled', 45, 'undefined', 8, '', NULL, '', 0),
	(74, '0', 45, 'undefined', 9, '', NULL, '', 0),
	(75, '0', 46, 'undefined', 8, '', NULL, '', 0),
	(76, '0', 46, 'undefined', 9, '', NULL, '', 0),
	(77, '0', 47, 'Pakodas - 12 oz', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(78, '0', 47, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(79, '0', 48, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(80, '0', 48, 'Coffee', 3, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(81, '0', 49, 'Paper Roast', 19, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(82, 'Cancelled', 49, 'Mini Idli - 12 Pcs', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(83, '0', 50, 'Pakodas - 12 oz', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(84, 'Cancelled', 50, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(85, '0', 51, 'Paper Roast', 19, '{"spice":3,"salt":1,"sour":1}', NULL, 'More Ghee', 1),
	(86, '0', 51, 'Pakodas - 12 oz', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'Pakoda less sakt', 1),
	(87, '0', 52, 'Plain Roast', 8, '{}', NULL, 'hello ashok dosa', 1),
	(88, 'Cancelled', 52, 'Paper Roast', 19, '{"spice":3,"salt":1,"sour":1}', NULL, 'Put more masala', 1),
	(89, '0', 53, 'Pakodas - 12 oz', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(90, '0', 53, 'Chilli Bajji - 5 Pcs', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(91, '0', 54, 'Pakodas - 12 oz', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(92, '0', 54, 'Rava Roast', 5, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(93, '0', 55, 'Pakodas - 12 oz', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'test 1', 1),
	(94, 'Cancelled', 55, 'Chilli Bajji - 5 Pcs', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'keep hot', 1),
	(95, '0', 56, 'undefined', 8, '', NULL, '', 1),
	(96, '0', 56, 'undefined', 10, '', NULL, '', 0),
	(97, '0', 57, 'undefined', 8, '', NULL, '', 1),
	(98, '0', 57, 'undefined', 10, '', NULL, '', 0),
	(99, '0', 58, 'undefined', 8, '', NULL, '', 1),
	(100, '0', 58, 'undefined', 10, '', NULL, '', 0),
	(101, '0', 59, 'undefined', 10, '', NULL, '', 0),
	(102, '0', 59, 'undefined', 9, '', NULL, '', 0),
	(103, '0', 60, 'undefined', 10, '', NULL, '', 0),
	(104, '0', 60, 'undefined', 9, '', NULL, '', 0),
	(105, '0', 61, 'Chilli Bajji - 5 Pcs', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(106, '0', 61, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(107, '0', 62, 'Mutton Biryani', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(108, '0', 62, 'Chicken Biryani', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(109, '0', 62, 'VEG - Biryani', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(110, '0', 63, 'Paneer Tikka', 9, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(111, '0', 63, 'Chana Masala', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(112, '0', 63, 'Mutton Gravy', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(113, '0', 64, 'Samosas - 5 Pcs', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(114, '0', 66, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(115, '0', 66, 'Samosas - 5 Pcs', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(116, '0', 66, 'Chilli Bajji - 5 Pcs', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(117, '0', 67, 'Mutton Gravy', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(118, '0', 68, 'Chicken Biryani', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(119, '0', 68, 'Palak Paneer', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(120, '0', 69, 'Chicken Biryani', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(121, '0', 69, 'Palak Paneer', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(122, '0', 70, 'Chicken Biryani', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(123, '0', 70, 'VEG - Biryani', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(124, '0', 71, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(125, '0', 71, 'Samosas - 5 Pcs', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(126, '0', 72, 'Samosas - 5 Pcs', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(127, '0', 73, 'Chilli Bajji - 5 Pcs', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(128, '0', 74, 'Samosas - 5 Pcs', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(129, '0', 75, 'Mutton Biryani', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(130, '0', 75, 'Chicken Biryani', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(131, '0', 76, 'Pakodas - 12 oz', 7, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(132, '0', 76, 'Gobi  Manchuri 12oz', 10, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0),
	(133, '0', 76, 'Chilli Bajji - 5 Pcs', 8, '{"spice":3,"salt":1,"sour":1}', NULL, 'undefined', 0);
/*!40000 ALTER TABLE `hmm_order_details` ENABLE KEYS */;


# Dumping structure for table projectsdb.hmm_sequence
DROP TABLE IF EXISTS `hmm_sequence`;
CREATE TABLE IF NOT EXISTS `hmm_sequence` (
  `sequence_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sequence_name` varchar(50) NOT NULL DEFAULT '',
  `sequence_number` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sequence_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.hmm_sequence: ~0 rows (approximately)
/*!40000 ALTER TABLE `hmm_sequence` DISABLE KEYS */;
/*!40000 ALTER TABLE `hmm_sequence` ENABLE KEYS */;


# Dumping structure for table projectsdb.hmm_stk_item
DROP TABLE IF EXISTS `hmm_stk_item`;
CREATE TABLE IF NOT EXISTS `hmm_stk_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `main_cat` varchar(50) NOT NULL DEFAULT '',
  `sub_cat` varchar(50) NOT NULL DEFAULT '',
  `price` varchar(20) DEFAULT '-1',
  `description` varchar(1500) NOT NULL DEFAULT '',
  `available_no` varchar(10) NOT NULL DEFAULT '0',
  `isAvailable` varchar(3) NOT NULL DEFAULT 'NO',
  `image` varchar(500) DEFAULT NULL,
  `contents` varchar(500) DEFAULT NULL,
  `branch_id` varchar(10) DEFAULT NULL,
  `addon` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.hmm_stk_item: ~34 rows (approximately)
/*!40000 ALTER TABLE `hmm_stk_item` DISABLE KEYS */;
INSERT INTO `hmm_stk_item` (`id`, `name`, `main_cat`, `sub_cat`, `price`, `description`, `available_no`, `isAvailable`, `image`, `contents`, `branch_id`, `addon`) VALUES
	(1, 'Ghee Roast', '', '6', '10.00', 'Same as the plain, but brushed with ghee (clarified butter).', '7', 'YES', 'gheeRoast.jpg', '{"spice":3,"salt":1,"sour":1}', '1', '[20,22]'),
	(2, 'Egg Dosa', '', '6', '9.00', 'What came first, the dosai or the egg? It\'s tasty and that\'s all that matters. We suggest not adding the spicy mix to this.', '8', 'YES', 'eggDosa.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(3, 'Paper Roast', '', '6', '9.00', 'A real thin variety of the plain dosai. Crispy served with coconut chutney and sambar.', '5', 'YES', 'paperDosa.jpg', '{"spice":3,"salt":1,"sour":1}', '1', '[1]'),
	(4, 'Plain Roast', '', '6', '7.00', 'Just the simple kind, no frills, but just as tasty served with coconut chutney and sambar1.', 'N', 'YES', 'plainDosa.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(5, 'Palak Paneer', '', '3', '7.00', 'Steam rice balls with spices as toppings served with veg oil coconut chutney and sambar.', 'N', 'YES', 'palakGravy.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(6, 'Chana Masala', '', '3', '8.00', 'Mini Steam rice balls served with coconut chutney and sambar.', 'N', 'YES', 'channaGravy.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(7, 'Paneer Tikka', '', '3', '9.00', 'Steam rice balls with butter as toppings  served with coconut chutney and sambar.', 'N', 'YES', 'paneerTikka.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(8, 'Black Tea', '', '8', '2.00', 'Tea with no milk', 'N', 'YES', 'blackTea.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(9, 'Masala Tea', '', '8', '3.00', 'Chai (tea, y\'all know that one!) made the traditional way with a hint of spices (masala made on premise) which brings out the flavor of the tea.', 'N', 'YES', 'masalaTea.jpg', '{"spice":3,"salt":1,"sour":1}', '2', NULL),
	(10, 'Coffee', '', '9', '3.00', 'Chennai dication filter coffee', 'N', 'YES', 'filterCoffee.jpg', '{"spice":3,"salt":1,"sour":1}', '2', NULL),
	(11, 'Pakodas - 12 oz', '', '1', '7.00', 'Crispy fried onions or Spinach dipped in a chickpea and rice flour batter. Just like amma used to make.', 'N', 'YES', 'pakoda.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(12, 'Samosas - 5 Pcs', '', '1', '7.00', 'Fried pastry with a savory filling of spiced potatoes and green peas. A pocket bursting with flavor.', 'N', 'YES', 'samosa.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(13, 'Gobi  Manchuri 12oz', '', '1', '10.00', 'துரையில் செல்போனுக்காக பிளஸ்2 மாணவர் கொலை மார்ச் 13,2017 06:24 IST', 'N', 'YES', 'gobiManchuri.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(14, 'Chilli Bajji - 5 Pcs', '', '1', '8.00', 'Chili and potato stuffing coated with batter and fried, served hot with tomato sauce or occasionally with mint and tamarind chutney.', 'N', 'YES', 'chilliBajji.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(15, 'Pineapple Kesari', '', '11', '4.00', 'Sooji (sweet cream of wheat), cooked with pineapple chunks and topped with nuts.', 'N', 'YES', 'pineappleKesari.jpg', '{"Sweet":3}', '1', NULL),
	(16, 'Double Ka Meeta', '', '11', '4.00', 'It\'s fried bread, soaked in milk and sprinkled with nuts, raisins and saffron.', 'N', 'YES', 'doubleKaMeeta.jpg', '{"Sweet":3}', '1', NULL),
	(17, 'Qubani Ka Meeta', '', '11', '5.00', 'Qubani which is nothing but apricots, boiled with syrup and chilled to the perfect consistency.', 'N', 'YES', 'qubaniKaMeeta.jpg', '{"Sweet":3}', '1', NULL),
	(18, 'VEG - Biryani', '', '2', '8.00', 'Plain dosai sprinkled with cheddar cheese.', '10', 'YES', 'vegBiryani.jpg', '{"spice":3,"salt":1,"sour":1}', '1', '[20,22]'),
	(19, 'Mango Lassi', '', '10', '4.00', 'Alphonso mango pulp blended with yogurt and served with a straw for the brave!', 'N', 'YES', 'mangoLassi.jpg', '{"Sweet":3}', '1', NULL),
	(20, 'Rose Milk', '', '10', '3.00', 'Rose flavored syrup blended with milk and ice cubes.', 'N', 'YES', 'roseMilk.jpg', '{"Sweet":3}', '1', NULL),
	(21, 'Soda and Water', '', '10', '1.00 each', 'Soda and Bottled water', 'N', 'YES', 'sodaWater.jpg', '{ }', '1', NULL),
	(22, 'Coke and Other Drinks', '', '10', '1.00 Eeach', 'Limca, Thumsup, Coke, Diet Coke, Sprite, Ginger Ale', 'N', 'YES', 'cokeOthers.jpg', '{ }', '1', NULL),
	(23, 'Sambar Rice Large Tray', '', '12', '99.00', 'Rice mixed with sambar and vegetables', 'N', 'YES', 'sambarRice.jpg', '{"spice":3,"salt":1}', '1', NULL),
	(24, 'Tomato Rice Large Tray', '', '12', '99.00', 'Rice mixed with tomatos', 'N', 'YES', 'tomatoRice.jpg', '{"spice":3,"salt":1}', '1', NULL),
	(25, '10 Varieties of Dosas - Unlimited', '', '13', '12 / Person', 'We come to your place and serve 10 different dosas and you can eat unlimited. The same great taste from our restaurant at the convience at your home party or event.', 'N', 'YES', 'dosaParty.jpg', '{"spice":3,"salt":1}', '1', NULL),
	(26, 'Sai Thokkran', '', '11', '5.00', 'Sai Thokkran, boiled with syrup and chilled to the perfect consistency.', 'N', 'YES', 'saiThokkaran.jpg', '{"Sweet":3}', '1', NULL),
	(28, 'Chicken Biryani', '', '2', '8.00', 'Plain dosai sprinkled with cheddar cheese.', '10', 'YES', 'chickenBiryani.jpg', '{"spice":3,"salt":1,"sour":1}', '1', '[20,22]'),
	(29, 'Mutton Biryani', '', '2', '8.00', 'Plain dosai sprinkled with cheddar cheese.', '10', 'YES', 'muttonBiryani.jpg', '{"spice":3,"salt":1,"sour":1}', '1', '[20,22]'),
	(30, 'Chicken Gravy', '', '4', '8.00', 'Plain dosai sprinkled with cheddar cheese.', '10', 'YES', 'chickenGravy.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(31, 'Mutton Gravy', '', '4', '8.00', 'Plain dosai sprinkled with cheddar cheese.', '10', 'YES', 'chickenGravy.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(32, 'Tandoor Chicken Tikka', '', '5', '8.00', 'Plain dosai sprinkled with cheddar cheese.', '10', 'YES', 'tikkaChick.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(33, 'Tandoor Malai Kabab', '', '5', '8.00', 'Plain dosai sprinkled with cheddar cheese.', '10', 'YES', 'malaichikken.jpg', '{"spice":3,"salt":1,"sour":1}', '1', NULL),
	(34, 'Plain Nan', '', '7', '5.00', 'Sai Thokkran, boiled with syrup and chilled to the perfect consistency.', 'N', 'YES', 'plainNaan.jpg', '{"Sweet":3}', '1', NULL),
	(35, 'Garlic Nan', '', '7', '5.00', 'Sai Thokkran, boiled with syrup and chilled to the perfect consistency.', 'N', 'NO', 'garlicNaan.jpg', '{"Sweet":3}', '1', NULL);
/*!40000 ALTER TABLE `hmm_stk_item` ENABLE KEYS */;


# Dumping structure for table projectsdb.hmm_subcat
DROP TABLE IF EXISTS `hmm_subcat`;
CREATE TABLE IF NOT EXISTS `hmm_subcat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) NOT NULL DEFAULT '0',
  `subcat_order` int(3) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.hmm_subcat: ~13 rows (approximately)
/*!40000 ALTER TABLE `hmm_subcat` DISABLE KEYS */;
INSERT INTO `hmm_subcat` (`id`, `cat_id`, `subcat_order`, `name`) VALUES
	(1, 1, 0, 'APPETIZERS'),
	(2, 1, 1, 'BIRYANI'),
	(3, 1, 2, 'VEG GRAVY'),
	(4, 1, 3, 'NON-VEG GRAVY'),
	(5, 1, 4, 'TANDOOORRRR'),
	(6, 1, 5, 'DOSA'),
	(7, 1, 6, 'NAAN (Breads)'),
	(8, 3, 7, 'TEA'),
	(9, 3, 8, 'COFFEE'),
	(10, 3, 9, 'COLD DRINKS'),
	(11, 2, 10, 'INDIAN DESSERTS'),
	(12, 4, 11, 'RICE VARITIES'),
	(13, 5, 12, 'DOSA PARTIES');
/*!40000 ALTER TABLE `hmm_subcat` ENABLE KEYS */;


# Dumping structure for table projectsdb.profile
DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pro_user_id` varchar(50) NOT NULL DEFAULT '',
  `pro_user_type` varchar(50) NOT NULL DEFAULT '',
  `pro_entity_id` varchar(50) NOT NULL DEFAULT '',
  `pro_is_active` varchar(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.profile: ~0 rows (approximately)
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;


# Dumping structure for table projectsdb.projects
DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `site` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.projects: ~0 rows (approximately)
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` (`id`, `name`, `site`, `description`) VALUES
	(1, 'Baluvvv', 'http://www.uuuytuy.com', 'ffgfgf');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;


# Dumping structure for table projectsdb.role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL DEFAULT '',
  `role_child` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.role: ~4 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `role_name`, `role_child`) VALUES
	(1, 'SuperAdmin', 'Asist'),
	(2, 'Admin', 'Test1'),
	(3, 'Customer', ''),
	(4, 'Guest', '');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


# Dumping structure for table projectsdb.rz_token_master
DROP TABLE IF EXISTS `rz_token_master`;
CREATE TABLE IF NOT EXISTS `rz_token_master` (
  `tok_id` int(10) DEFAULT NULL,
  `usr_id` varchar(50) DEFAULT NULL,
  `tok_logo` blob,
  `tok_entity_id` int(10) DEFAULT NULL,
  `tok_name` varchar(50) DEFAULT NULL,
  `tok_type` varchar(50) DEFAULT NULL,
  `tok_valid_from` date DEFAULT NULL,
  `tok_valid_to` date DEFAULT NULL,
  `tok_current` varchar(50) DEFAULT NULL,
  `tok_start` int(10) DEFAULT NULL,
  `tok_end` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.rz_token_master: ~0 rows (approximately)
/*!40000 ALTER TABLE `rz_token_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `rz_token_master` ENABLE KEYS */;


# Dumping structure for table projectsdb.rz_token_run
DROP TABLE IF EXISTS `rz_token_run`;
CREATE TABLE IF NOT EXISTS `rz_token_run` (
  `tok_run_id` int(10) DEFAULT NULL,
  `tok_entity_id` int(10) DEFAULT NULL,
  `tok_run_tok_name` varchar(50) DEFAULT NULL,
  `usr_id` varchar(50) DEFAULT NULL,
  `tok_desc` varchar(50) DEFAULT NULL,
  `tok_used` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.rz_token_run: ~0 rows (approximately)
/*!40000 ALTER TABLE `rz_token_run` DISABLE KEYS */;
/*!40000 ALTER TABLE `rz_token_run` ENABLE KEYS */;


# Dumping structure for table projectsdb.token
DROP TABLE IF EXISTS `token`;
CREATE TABLE IF NOT EXISTS `token` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tkn_ent_id` varchar(50) NOT NULL DEFAULT '',
  `tkn_count` varchar(50) NOT NULL DEFAULT '',
  `tkn_start` varchar(50) NOT NULL DEFAULT '',
  `tkn_name` varchar(200) NOT NULL DEFAULT '',
  `tkn_date` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.token: ~0 rows (approximately)
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
/*!40000 ALTER TABLE `token` ENABLE KEYS */;


# Dumping structure for table projectsdb.token_details
DROP TABLE IF EXISTS `token_details`;
CREATE TABLE IF NOT EXISTS `token_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tkn_id` varchar(50) NOT NULL DEFAULT '',
  `tkn_name` varchar(50) NOT NULL DEFAULT '',
  `tkn_user` varchar(50) NOT NULL DEFAULT '',
  `tkn_done` varchar(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.token_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `token_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `token_details` ENABLE KEYS */;


# Dumping structure for table projectsdb.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '',
  `usr_ent_id` int(10) NOT NULL DEFAULT '0',
  `role` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) DEFAULT '',
  `email` varchar(50) DEFAULT NULL,
  `phonenumber` varchar(200) DEFAULT '',
  `invite` tinyint(1) DEFAULT NULL,
  `verified` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.users: ~7 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `usr_ent_id`, `role`, `password`, `email`, `phonenumber`, `invite`, `verified`) VALUES
	(6, '2', 4, '0', '123123', 'asd@werw.rth.gh', '2134123', NULL, NULL),
	(9, 'Balusamy Nachiappan', 3, 'HAdmin', '2', 'tnbalu@yahoo.com', '5072164591', NULL, NULL),
	(10, 'Balusamy Nachiappan', 3, 'HEmployee', '1', 'tnbalu@yaho2wo.com', '5072164591', NULL, NULL),
	(11, 'Balusamy', 5, 'SAdmin', '1', 'p2nbalu@gmail.com', '+919790300039', NULL, NULL),
	(12, 'Balusamy Nachiappan', 1, 'Customer', '12', 'tnbalu@yah2oo.com', '50721645911', NULL, NULL),
	(13, 'sdsfsdf', 0, '', 'werwerw', 'sdfsdfs@sdfs.com', '345', NULL, NULL),
	(14, '444444444', 0, '', '5', 'pnbalu@yahoo.com', '666666666', NULL, NULL),
	(21, 'rrrrrrrr', 0, '', '4', 'pnbalu@gmail.com', '4', NULL, NULL),
	(22, 'Raja', 0, '', 'I am ', 'King@yahoo.com', '000000000', NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


# Dumping structure for table projectsdb.user_entity
DROP TABLE IF EXISTS `user_entity`;
CREATE TABLE IF NOT EXISTS `user_entity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usr_ent_user_id` varchar(50) NOT NULL DEFAULT '',
  `usr_ent_name` varchar(50) NOT NULL DEFAULT '',
  `usr_role_name` varchar(50) NOT NULL DEFAULT '',
  `usr_ent_address1` varchar(200) NOT NULL DEFAULT '',
  `usr_ent_address2` varchar(200) NOT NULL DEFAULT '',
  `usr_ent_city` varchar(200) NOT NULL DEFAULT '',
  `usr_ent_state` varchar(200) NOT NULL DEFAULT '',
  `usr_ent_zip` varchar(200) NOT NULL DEFAULT '',
  `usr_ent_is_active` varchar(1) NOT NULL DEFAULT '',
  `usr_ent_currency` varchar(5) DEFAULT 'US$',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.user_entity: ~2 rows (approximately)
/*!40000 ALTER TABLE `user_entity` DISABLE KEYS */;
INSERT INTO `user_entity` (`id`, `usr_ent_user_id`, `usr_ent_name`, `usr_role_name`, `usr_ent_address1`, `usr_ent_address2`, `usr_ent_city`, `usr_ent_state`, `usr_ent_zip`, `usr_ent_is_active`, `usr_ent_currency`) VALUES
	(1, '2', 'My Hotel1', 'Doctor', 'asdf', 'qeqw', 'qw', 'qw', 'qwqw', '1', 'US$'),
	(2, '2', 'Appolo', 'Doctor', '876', '876', '876', '876', '876', '1', 'US$');
/*!40000 ALTER TABLE `user_entity` ENABLE KEYS */;


# Dumping structure for table projectsdb.vv_menu
DROP TABLE IF EXISTS `vv_menu`;
CREATE TABLE IF NOT EXISTS `vv_menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) NOT NULL DEFAULT '',
  `menu_url` varchar(500) NOT NULL DEFAULT '',
  `menu_submenu` varchar(50) NOT NULL DEFAULT '',
  `menu_order` int(10) unsigned NOT NULL DEFAULT '0',
  `menu_imgurl` varchar(500) NOT NULL DEFAULT '',
  `menu_type` varchar(10) NOT NULL DEFAULT '',
  `menu_role` varchar(10) NOT NULL DEFAULT '',
  `menu_comp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.vv_menu: ~11 rows (approximately)
/*!40000 ALTER TABLE `vv_menu` DISABLE KEYS */;
INSERT INTO `vv_menu` (`menu_id`, `menu_name`, `menu_url`, `menu_submenu`, `menu_order`, `menu_imgurl`, `menu_type`, `menu_role`, `menu_comp_id`) VALUES
	(1, 'Administration', 'admin', '', 1, 'fa-dashboard', '', '', NULL),
	(2, 'Entity Role', 'entity_role.html', '', 3, 'fa-laptop', '', '', NULL),
	(3, 'Role', 'role.html', 'ssssssssss', 3, 'profile', '', '', NULL),
	(4, 'Dashboard', 'dashboard.html', '', 3, '', '', '', NULL),
	(6, 'Hotel Administration', 'stock', '', 2, 'fa-dashboard', '', '', NULL),
	(7, 'Branches', 'userInfo', '', 3, 'fa-th', '', '', NULL),
	(11, 'Order', '/order', 'Online Order', 3, '', 'Menu', '["HAdmin"]', NULL),
	(12, 'Order', '/inorder', 'Walk-In', 3, '', 'Menu', '["HAdmin"]', NULL),
	(13, 'Order', '/t-order', 'Table Order', 3, '', 'Menu', '["HAdmin"]', NULL),
	(14, 'Management', '/foodmgt', 'Food Management', 3, '', 'Menu', '["HAdmin"]', NULL),
	(15, 'Management', '/branchmgt', 'Branch  Management', 3, '', 'Menu', '["HAdmin"]', NULL),
	(16, 'Management', '/food', 'Food', 3, '', 'Menu', '["HAdmin"]', NULL),
	(17, 'Management', '/graphmgt', 'Graph', 3, '', 'Menu', '["HAdmin"]', NULL);
/*!40000 ALTER TABLE `vv_menu` ENABLE KEYS */;


# Dumping structure for table projectsdb.vv_role
DROP TABLE IF EXISTS `vv_role`;
CREATE TABLE IF NOT EXISTS `vv_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL DEFAULT '',
  `role_child` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.vv_role: ~0 rows (approximately)
/*!40000 ALTER TABLE `vv_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `vv_role` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
