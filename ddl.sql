# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.7.10-log
# Server OS:                    Win64
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2017-02-20 19:27:03
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping database structure for projectsdb
DROP DATABASE IF EXISTS `projectsdb`;
CREATE DATABASE IF NOT EXISTS `projectsdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `projectsdb`;


# Dumping structure for table projectsdb.hmm_branch
DROP TABLE IF EXISTS `hmm_branch`;
CREATE TABLE IF NOT EXISTS `hmm_branch` (
  `branch_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(10) NOT NULL DEFAULT '0',
  `branch_name` varchar(50) NOT NULL DEFAULT '',
  `branch_address1` varchar(200) NOT NULL DEFAULT '',
  `branch_latitude` varchar(100) NOT NULL DEFAULT '',
  `branch_longitude` varchar(100) NOT NULL DEFAULT '',
  `branch_address2` varchar(200) NOT NULL DEFAULT '',
  `branch_city` varchar(20) NOT NULL DEFAULT '',
  `branch_state` varchar(20) NOT NULL DEFAULT '',
  `branch_zip` varchar(10) NOT NULL DEFAULT '',
  `branch_is_active` varchar(1) NOT NULL DEFAULT '',
  `branch_start_time` varchar(5) NOT NULL DEFAULT '',
  `branch_end_time` varchar(5) NOT NULL DEFAULT '',
  `branch_is_closed` varchar(1) NOT NULL DEFAULT '',
  `branch_phone` varchar(13) NOT NULL DEFAULT '',
  `branch_map` varchar(200) NOT NULL DEFAULT '',
  `branch_currency` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.hmm_branch: ~2 rows (approximately)
/*!40000 ALTER TABLE `hmm_branch` DISABLE KEYS */;
INSERT INTO `hmm_branch` (`branch_id`, `company_id`, `branch_name`, `branch_address1`, `branch_latitude`, `branch_longitude`, `branch_address2`, `branch_city`, `branch_state`, `branch_zip`, `branch_is_active`, `branch_start_time`, `branch_end_time`, `branch_is_closed`, `branch_phone`, `branch_map`, `branch_currency`) VALUES
	(1, 1, 'Branch1', 'uytr', '51.508515', '-0.125487', 'uyt', 'uyt', 'uyt', '600134', 'Y', '12:34', '11:50', 'N', '654765754', '', '$'),
	(2, 1, 'Branch2', 'uytr', '52.370216', '4.895168', 'uyt', 'uyt', 'uyt', '600134', 'Y', '12:34', '11:50', 'N', '654765754', '', '$');
/*!40000 ALTER TABLE `hmm_branch` ENABLE KEYS */;


# Dumping structure for table projectsdb.hmm_cat
DROP TABLE IF EXISTS `hmm_cat`;
CREATE TABLE IF NOT EXISTS `hmm_cat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `cat_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.hmm_cat: ~2 rows (approximately)
/*!40000 ALTER TABLE `hmm_cat` DISABLE KEYS */;
INSERT INTO `hmm_cat` (`id`, `name`, `cat_order`) VALUES
	(1, 'FOOD', 1),
	(2, 'BEVARAGES', 2),
	(3, 'Cake', 3);
/*!40000 ALTER TABLE `hmm_cat` ENABLE KEYS */;


# Dumping structure for table projectsdb.hmm_company
DROP TABLE IF EXISTS `hmm_company`;
CREATE TABLE IF NOT EXISTS `hmm_company` (
  `company_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) NOT NULL DEFAULT '',
  `company_slogan` varchar(200) DEFAULT NULL,
  `company_url` varchar(200) DEFAULT NULL,
  `company_image` varchar(500) DEFAULT NULL,
  `company_logo` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.hmm_company: ~0 rows (approximately)
/*!40000 ALTER TABLE `hmm_company` DISABLE KEYS */;
INSERT INTO `hmm_company` (`company_id`, `company_name`, `company_slogan`, `company_url`, `company_image`, `company_logo`) VALUES
	(1, 'BriyaniCity', 'Taste the Best', 'www.briyaniCity.com', NULL, 'bc_logo.png');
/*!40000 ALTER TABLE `hmm_company` ENABLE KEYS */;


# Dumping structure for table projectsdb.hmm_stk_item
DROP TABLE IF EXISTS `hmm_stk_item`;
CREATE TABLE IF NOT EXISTS `hmm_stk_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `main_cat` varchar(50) NOT NULL DEFAULT '',
  `sub_cat` varchar(50) NOT NULL DEFAULT '',
  `price` varchar(10) DEFAULT '-1',
  `description` varchar(1500) NOT NULL DEFAULT '',
  `available_no` varchar(10) NOT NULL DEFAULT '0',
  `isAvailable` varchar(3) NOT NULL DEFAULT 'NO',
  `image` varchar(500) DEFAULT NULL,
  `contents` varchar(500) DEFAULT NULL,
  `branch_id` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.hmm_stk_item: ~14 rows (approximately)
/*!40000 ALTER TABLE `hmm_stk_item` DISABLE KEYS */;
INSERT INTO `hmm_stk_item` (`id`, `name`, `main_cat`, `sub_cat`, `price`, `description`, `available_no`, `isAvailable`, `image`, `contents`, `branch_id`) VALUES
	(1, 'Cheese Dosa', '', '1', '5', 'Plain dosai sprinkled with cheddar cheese served with coconut chutney and sambar.', '10', 'NO', 'cheeseDosa.jpg', '{"spice":3,"salt":1,"sour":1}', '1'),
	(2, 'Rava Roast', '', '1', '5.30', 'Same as the plain, but brushed with ghee (clarified butter)', '11', 'NO', 'ravaDosa.jpg', '{"spice":3,"salt":1,"sour":1}', '1'),
	(3, 'Paper Roast', '', '1', '19.00', 'A real thin variety of the plain dosai. Crispy served with coconut chutney and sambar.', 'N', 'NO', 'paperDosa.jpg', '{"spice":3,"salt":1,"sour":1}', '1'),
	(4, 'Plain Roast', '', '1', '8.00', 'Just the simple kind, no frills, but just as tasty served with coconut chutney and sambar.', 'N', 'NO', 'plainDosa.jpg', '', '1'),
	(5, 'Podi Idli - 2 Pcs', '', '2', '7.00', 'Steam rice balls with spices as toppings served with veg oil coconut chutney and sambar.', 'N', 'YES', 'podiIdli.jpg', '{"spice":3,"salt":1,"sour":1}', '1'),
	(6, 'Mini Idli - 12 Pcs', '', '2', '8.00', 'Mini Steam rice balls served with coconut chutney and sambar.', 'N', '', 'miniIdli.jpg', '{"spice":3,"salt":1,"sour":1}', '1'),
	(7, 'Butter Idli - 2 Pcs', '', '2', '9.00', 'Steam rice balls with butter as toppings  served with coconut chutney and sambar.', 'N', 'YES', 'butterIdli.jpg', '{"spice":3,"salt":1,"sour":1}', '1'),
	(8, 'Black Tea', '', '3', '2.00', 'Tea with no milk', 'N', 'YES', 'blackTea.jpg', '{"spice":3,"salt":1,"sour":1}', '1'),
	(9, 'Masala Tea', '', '3', '3.00', 'Chai (tea, y\'all know that one!) made the traditional way with a hint of spices (masala made on premise) which brings out the flavor of the tea.', 'N', '', 'masalaTea.jpg', '{"spice":3,"salt":1,"sour":1}', '1'),
	(10, 'Coffee', '', '4', '3.00', 'Chennai dication filter coffee', 'N', '', 'filterCoffee.jpg', '{"spice":3,"salt":1,"sour":1}', '1'),
	(11, 'Pakodas - 12 oz', '', '5', '7.00', 'Crispy fried onions or Spinach dipped in a chickpea and rice flour batter. Just like amma used to make.', 'N', 'YES', 'pakoda.jpg', '{"spice":3,"salt":1,"sour":1}', '1'),
	(12, 'Samosas - 5 Pcs', '', '5', '7.00', 'Fried pastry with a savory filling of spiced potatoes and green peas. A pocket bursting with flavor.', 'N', '', 'samosa.jpg', '{"spice":3,"salt":1,"sour":1}', '1'),
	(13, 'Gobi  Manchuri 12oz', '', '5', '10.00', 'Coated in a Manchurian sauce made from scratch (ginger and garlic, soy sauce, chili sauces, and vinegar)', 'N', 'YES', 'gobiManchuri.jpg', '{"spice":3,"salt":1,"sour":1}', '1'),
	(14, 'Chilli Bajji - 5 Pcs', '', '5', '8.00', 'Chili and potato stuffing coated with batter and fried, served hot with tomato sauce or occasionally with mint and tamarind chutney.', 'N', 'YES', 'chilliBajji.jpg', '{"spice":3,"salt":1,"sour":1}', '1');
/*!40000 ALTER TABLE `hmm_stk_item` ENABLE KEYS */;


# Dumping structure for table projectsdb.hmm_subcat
DROP TABLE IF EXISTS `hmm_subcat`;
CREATE TABLE IF NOT EXISTS `hmm_subcat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) NOT NULL DEFAULT '0',
  `subcat_order` int(3) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.hmm_subcat: ~4 rows (approximately)
/*!40000 ALTER TABLE `hmm_subcat` DISABLE KEYS */;
INSERT INTO `hmm_subcat` (`id`, `cat_id`, `subcat_order`, `name`) VALUES
	(1, 1, 1, 'DOSA'),
	(2, 1, 2, 'IDLI'),
	(3, 2, 3, 'TEA'),
	(4, 2, 4, 'COFFE'),
	(5, 1, 0, 'APPETIZERS');
/*!40000 ALTER TABLE `hmm_subcat` ENABLE KEYS */;


# Dumping structure for table projectsdb.profile
DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pro_user_id` varchar(50) NOT NULL DEFAULT '',
  `pro_user_type` varchar(50) NOT NULL DEFAULT '',
  `pro_entity_id` varchar(50) NOT NULL DEFAULT '',
  `pro_is_active` varchar(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.profile: ~0 rows (approximately)
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;


# Dumping structure for table projectsdb.projects
DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `site` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.projects: ~0 rows (approximately)
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` (`id`, `name`, `site`, `description`) VALUES
	(1, 'Baluvvv', 'http://www.uuuytuy.com', 'ffgfgf');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;


# Dumping structure for table projectsdb.role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL DEFAULT '',
  `role_child` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.role: ~4 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `role_name`, `role_child`) VALUES
	(1, 'SuperAdmin', 'Asist'),
	(2, 'Admin', 'Test1'),
	(3, 'Customer', ''),
	(4, 'Guest', '');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


# Dumping structure for table projectsdb.rz_token_master
DROP TABLE IF EXISTS `rz_token_master`;
CREATE TABLE IF NOT EXISTS `rz_token_master` (
  `tok_id` int(10) DEFAULT NULL,
  `usr_id` varchar(50) DEFAULT NULL,
  `tok_logo` blob,
  `tok_entity_id` int(10) DEFAULT NULL,
  `tok_name` varchar(50) DEFAULT NULL,
  `tok_type` varchar(50) DEFAULT NULL,
  `tok_valid_from` date DEFAULT NULL,
  `tok_valid_to` date DEFAULT NULL,
  `tok_current` varchar(50) DEFAULT NULL,
  `tok_start` int(10) DEFAULT NULL,
  `tok_end` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.rz_token_master: ~0 rows (approximately)
/*!40000 ALTER TABLE `rz_token_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `rz_token_master` ENABLE KEYS */;


# Dumping structure for table projectsdb.rz_token_run
DROP TABLE IF EXISTS `rz_token_run`;
CREATE TABLE IF NOT EXISTS `rz_token_run` (
  `tok_run_id` int(10) DEFAULT NULL,
  `tok_entity_id` int(10) DEFAULT NULL,
  `tok_run_tok_name` varchar(50) DEFAULT NULL,
  `usr_id` varchar(50) DEFAULT NULL,
  `tok_desc` varchar(50) DEFAULT NULL,
  `tok_used` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.rz_token_run: ~0 rows (approximately)
/*!40000 ALTER TABLE `rz_token_run` DISABLE KEYS */;
/*!40000 ALTER TABLE `rz_token_run` ENABLE KEYS */;


# Dumping structure for table projectsdb.token
DROP TABLE IF EXISTS `token`;
CREATE TABLE IF NOT EXISTS `token` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tkn_ent_id` varchar(50) NOT NULL DEFAULT '',
  `tkn_count` varchar(50) NOT NULL DEFAULT '',
  `tkn_start` varchar(50) NOT NULL DEFAULT '',
  `tkn_name` varchar(200) NOT NULL DEFAULT '',
  `tkn_date` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.token: ~0 rows (approximately)
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
/*!40000 ALTER TABLE `token` ENABLE KEYS */;


# Dumping structure for table projectsdb.token_details
DROP TABLE IF EXISTS `token_details`;
CREATE TABLE IF NOT EXISTS `token_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tkn_id` varchar(50) NOT NULL DEFAULT '',
  `tkn_name` varchar(50) NOT NULL DEFAULT '',
  `tkn_user` varchar(50) NOT NULL DEFAULT '',
  `tkn_done` varchar(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.token_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `token_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `token_details` ENABLE KEYS */;


# Dumping structure for table projectsdb.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '',
  `usr_ent_id` int(10) NOT NULL DEFAULT '0',
  `role` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) DEFAULT '',
  `email` varchar(50) DEFAULT NULL,
  `phonenumber` varchar(200) DEFAULT '',
  `invite` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.users: ~6 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `usr_ent_id`, `role`, `password`, `email`, `phonenumber`, `invite`) VALUES
	(6, '2', 4, '0', '123123', 'asd@werw.rth.gh', '2134123', NULL),
	(9, 'Balusamy Nachiappan', 3, '2', '2', 'tnbalu@yahoo.com', '5072164591', NULL),
	(10, 'Balusamy Nachiappan', 3, '0', '1', 'tnbalu@yaho2wo.com', '5072164591', NULL),
	(11, 'Balusamy', 5, '0', '1', 'pnbalu@gmail.com', '+919790300039', NULL),
	(12, 'Balusamy Nachiappan', 1, '1', '12', 'tnbalu@yah2oo.com', '50721645911', NULL),
	(13, 'sdsfsdf', 0, '', 'werwerw', 'sdfsdfs@sdfs.com', '345', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


# Dumping structure for table projectsdb.user_entity
DROP TABLE IF EXISTS `user_entity`;
CREATE TABLE IF NOT EXISTS `user_entity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usr_ent_user_id` varchar(50) NOT NULL DEFAULT '',
  `usr_ent_name` varchar(50) NOT NULL DEFAULT '',
  `usr_role_name` varchar(50) NOT NULL DEFAULT '',
  `usr_ent_address1` varchar(200) NOT NULL DEFAULT '',
  `usr_ent_address2` varchar(200) NOT NULL DEFAULT '',
  `usr_ent_city` varchar(200) NOT NULL DEFAULT '',
  `usr_ent_state` varchar(200) NOT NULL DEFAULT '',
  `usr_ent_zip` varchar(200) NOT NULL DEFAULT '',
  `usr_ent_is_active` varchar(1) NOT NULL DEFAULT '',
  `usr_ent_currency` varchar(5) DEFAULT 'US$',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.user_entity: ~2 rows (approximately)
/*!40000 ALTER TABLE `user_entity` DISABLE KEYS */;
INSERT INTO `user_entity` (`id`, `usr_ent_user_id`, `usr_ent_name`, `usr_role_name`, `usr_ent_address1`, `usr_ent_address2`, `usr_ent_city`, `usr_ent_state`, `usr_ent_zip`, `usr_ent_is_active`, `usr_ent_currency`) VALUES
	(1, '2', 'My Hotel1', 'Doctor', 'asdf', 'qeqw', 'qw', 'qw', 'qwqw', '1', 'US$'),
	(2, '2', 'Appolo', 'Doctor', '876', '876', '876', '876', '876', '1', 'US$');
/*!40000 ALTER TABLE `user_entity` ENABLE KEYS */;


# Dumping structure for table projectsdb.vv_menu
DROP TABLE IF EXISTS `vv_menu`;
CREATE TABLE IF NOT EXISTS `vv_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) NOT NULL DEFAULT '',
  `menu_url` varchar(500) NOT NULL DEFAULT '',
  `menu_submenu` varchar(50) NOT NULL DEFAULT '',
  `menu_order` int(10) unsigned NOT NULL DEFAULT '0',
  `menu_imgurl` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.vv_menu: ~7 rows (approximately)
/*!40000 ALTER TABLE `vv_menu` DISABLE KEYS */;
INSERT INTO `vv_menu` (`id`, `menu_name`, `menu_url`, `menu_submenu`, `menu_order`, `menu_imgurl`) VALUES
	(1, 'Administration', 'admin', '', 1, 'fa-dashboard'),
	(2, 'Entity Role', 'entity_role.html', '', 3, 'fa-laptop'),
	(3, 'Role', 'role.html', 'ssssssssss', 3, 'profile'),
	(4, 'Dashboard', 'dashboard.html', '', 3, ''),
	(6, 'Hotel Administration', 'stock', '', 2, 'fa-dashboard'),
	(7, 'Branches', 'userInfo', '', 3, 'fa-th'),
	(11, 'ssss', '', '', 3, '');
/*!40000 ALTER TABLE `vv_menu` ENABLE KEYS */;


# Dumping structure for table projectsdb.vv_role
DROP TABLE IF EXISTS `vv_role`;
CREATE TABLE IF NOT EXISTS `vv_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL DEFAULT '',
  `role_child` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

# Dumping data for table projectsdb.vv_role: ~5 rows (approximately)
/*!40000 ALTER TABLE `vv_role` DISABLE KEYS */;
INSERT INTO `vv_role` (`id`, `role_name`, `role_child`) VALUES
	(1, 'Superadmin', ''),
	(2, 'Admin', ''),
	(3, 'Customer', ''),
	(4, 'Guest', ''),
	(5, 'Attender', '');
/*!40000 ALTER TABLE `vv_role` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
