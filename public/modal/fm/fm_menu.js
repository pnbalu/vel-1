var connection;
exports.setupDBAndTable = function (conn) {

    //save connection
    connection = conn;

    //NOTE: This particular (https://github.com/felixge/node-mysql) Mysql module,
    // queues ALL 'connection.query' calls & runs queries in SEQUENCE so we don't have to nest them!
    //i.e. In the below code,
    //1. create usersDB DB & *its* callback is called FIRST,
    //2. then 'use usersDB' and *its callback* is called
    //3. then show tables query and *its callback* is called
    //4. then 'create' table is called

    //If not on Cloud Foundry, create DB 'usersDB' and then switch to it 'usersDB'
    //Note: Cloud Foundry does these steps automatically.
    if (!process.env.VCAP_SERVICES) {
        connection.query('CREATE DATABASE IF NOT EXISTS projectsDB;', function (err) {
            if (err)  return console.log(err);
        });

        //Switch to 'usersDB' database
        connection.query('USE  projectsDB;', function (err) {
            if (err)  return console.log(err);
        });
    }

};

exports.addUser = function (task, callback) {
    connection.query("INSERT INTO users (name, password, email,phonenumber) VALUES (?, ?, ?,?)", [task.name, task.password, task.email,task.phonenumber], 
	 callback);
	console.log(task)
};

exports.updateUser = function (id, task, callback) {
    var sql = "UPDATE users SET name='" + task.name
        + "', password='" + task.password
        + "', email='" + task.email
		+ "', phonenumber='" + task.phonenumber
        + "' WHERE id=" + id;
console.log(sql)
    connection.query(sql, callback);
};

exports.getUsers = function (callback) {
    connection.query("SELECT * FROM users", callback);
};

exports.getData = function (entity,callback) {
    connection.query("SELECT * FROM "+entity, callback);
};

exports.getUser = function (id,password, callback) {
	var sql="SELECT * FROM users WHERE email='" +id+ "' and password='" +password+ "'";
	console.log('============='+sql);
    connection.query(sql, callback);
};

exports.deleteUser = function (id, callback) {
    connection.query("DELETE FROM users WHERE id=" + id, callback);
};