app.directive('vvUser', ['$document','$rootScope','$http','FmService', function($document,$rootScope,$http,FmService) {
    'use strict';

   
    return {
      restrict: 'EA',
      templateUrl:'/vv-fm/directive/vv_user.html',
      scope: {
        orderdetail: '=' ,
         user: '='       
      },
      link: function(scope, element, attributes) {  
        scope.showCart=function(){
            $rootScope.leftmenu
          if( $rootScope.enablecart){
             $rootScope.enablecart=false
          }else{
             $rootScope.enablecart=true
          }
        }     
        scope.saveUser=function(){
          if(scope.user.email){

          }
          $http({
            method : 'POST',
            url : "/saveuser",
            data:{input:scope.user},
          }).then(function successCallback(response) {
            console.log(response)
           
          }, function errorCallback(response) {
            
          });
        }
        /*
        *
        */
        scope.login=function(){
          $http({
            method : 'GET',
            url : "/login",
            data:{input:scope.user},
          }).then(function successCallback(response) {
            FmService.setUser(response)  
            scope.user_role=response.role;        
          }, function errorCallback(response) {
            
          });
        }
      }
    };

}]);
