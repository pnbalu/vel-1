angular.module('PVM.Util3',[]).directive('vvUpsert', ['$document','$rootScope','$http', function($document,$rootScope,$http) {
    'use strict';

   
    return {
       restrict: 'EA',
        scope: { vconfig: '=',vdata:'='},
        link: function (scope, element, attrs, ngModel) {
			scope.vf={vfconfig:{name:{type:'text',dispName:'Name'},age:{type:'number',dispName:'Age'},sex:{type:'radio',dispName:'Sex'},dob:{type:'date',dispName:'Date Of Birth'}}}
	        var url = "";

            //if(!vconfig || !vconfig.url){
                url='http://localhost:3000/vvsavel';
            //}
            scope.vv_save=function(){
            	console.log(scope.vdata);
				$http({
					method: 'POST',
					url: url,
					data:{data:scope.vdata,config:scope.vconfig}	
				}).then(function successCallback(response) {               
					//$scope.menuData = response.data;
				}, function errorCallback(response) {

				});	
                /*$http({
                    method: 'POST',
                  url: 'http://localhost:3000/vvdata',
                  data:{entity:'vv_menu'}   
                }).then(function successCallback(response) {               
                    
                }, function errorCallback(response) {
                   
                });*/
                
            }
        },
        template:function(tElem, tAttrs){               
                var formTemplate=' <div style="align:right" class="vv_upsert">'+
								'	<button class="btn" type="button" value="Save" ng-click="vv_save()" tabindex="0">Save</button>'+
								'	<button class="btn" type="button" value="Edit" ng-click="vv_edit()" tabindex="0">Edit</button>	'+
								'</div>'            
            return formTemplate;
        }
    };

}]);
