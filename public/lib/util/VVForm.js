
angular.module('PVM.Util1',[]).directive('vvForms', ['$document','$rootScope', function($document,$rootScope) {
    'use strict';

   
    return {
       restrict: 'EA',
        scope: { vconfig: '=',vdata:'=',vparent:'=',pscope:'='},
        link: function (scope, element, attrs, ngModel) {
			scope.vf={vfconfig:{name:{type:'text',dispName:'Name'},age:{type:'number',dispName:'Age'},sex:{type:'radio',dispName:'Sex'},dob:{type:'date',dispName:'Date Of Birth'}}}
	 
            console.log(scope.vf);
        },
        template:function(tElem, tAttrs){               
                var formTemplate='<div class="box box-info">'+
                                 '   <div class="box-header with-border">'+
                                 '      <h3 class="box-title">Register</h3>'+
                                 '    </div>'+
                                   
                                 '    <form class="form-horizontal">'+
                                 '     <div class="box-body" ng-repeat="(key,value) in vf.vfconfig">'+
                                 '       <div class="form-group" >'+
                                 '         <label for="inputEmail3" class="col-sm-2 control-label">{{value.dispName}}</label>'+
                                 '          <div class="col-sm-10">'+
                                 '           <input type="value.type" class="form-control" ng-model="key" id="key" >'+
                                 '        </div>'+
                                 '        </div>'+
                                '      </div>'+                                     
                                '      <div class="box-footer">'+
                                '        <button type="submit" class="btn btn-default">Cancel</button>'+
                                '        <button type="submit" class="btn btn-info pull-right">Sign in</button>'+
                                '      </div>'+                                     
                                '    </form>'+
                                '  </div>'
            var ptemplate='<div class=""><input type="date"    ng-model="seldate"  ng-blur="updatedateDate($event)"> </div>   ';
            return formTemplate;
        }
    };

}]);

