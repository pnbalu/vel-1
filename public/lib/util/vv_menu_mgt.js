app.directive('vvMenuMgt', ['$document','$rootScope','$http','FmService','$location', function($document,$rootScope,$http,FmService,$location) {
    'use strict';   
    return {
       restrict: 'EA',
        scope: { vconfig: '=',vdata:'='},
        link: function (scope, element, attrs, ngModel) {			
            function convertContent(obj){
                for (var i = 0; i < obj.length; i++) {
                    if(obj[i].ingrediants){                        
                        console.log(obj[i]);
                        obj[i].ingrediants=JSON.parse(obj[i].ingrediants);
                    }   
                }
                console.log(obj)
                return obj;
            }
            function contructMenu(mdata,parent){
                var menus = mdata;              
                for (var i = 0; i < menus.length; i++) {                  
                    
                    var menu=menus[i].menu;
                    var tempmenu;

                    var sub_menu=menus[i].menu_item;
                    var sub_tempmenu;

                    if(tempmenu != menu){ 
                        var p_menus={};                           
                        p_menus.menu=menu;
                        p_menus.submenu=[];
                        parent.push(p_menus); 
                        tempmenu=menu                                                                               
                    }

                    if(sub_tempmenu != sub_menu){ 
                        p_menus.submenu.push(menus[i]);
                        sub_tempmenu=sub_menu                                                        
                    }                        
                    console.log(parent);
                }
                return parent
            }

	            var url = "";
                var roleA=[FmService.getRole()]
				$http({
					method: 'POST',
					url: '/fm_menu',
					data:{role:JSON.stringify(roleA)}	
				}).then(function successCallback(response) {               
					var parent=[]
                    scope.parent=contructMenu(convertContent(response.data),parent)
                    $rootScope.pMenu=response.data
                   // $location.path('/order');
				}, function errorCallback(response) {

				});	 
                
                 scope.displaysubmenu=function(menu){
                     $location.path(menu.menu_url);
                }
                scope.searchFood=function(menu){
                    $rootScope.selectedfood=scope.searchMenu
                }
        },
        template:function(tElem, tAttrs){               
                var formTemplate = '<ul class="sidebar-menu">'+
                                 '       <li class="header">MAIN NAVIGATION</li>'+
                                 '       <li class="active treeview" ng-repeat="menu in menus">'+
                                 '         <a href="#/{{menu.menu_url}}">'+
                                 '           <i class="fa {{menu.menu_imgurl}}"></i> <span>{{menu.menu_name}}</span>'+
                                 '         </a>'+
                                 '       </li>'
                                 '</ul>';

                var formAdminTemplate = 
                                
                                 '<ul class="sidebar-menu">'+                                
                                 '       <li class=" treeview active" ng-repeat="menu in parent">'+
                                 '         <a class="fa  fa-cutlery " href="void">'+
                                 '           <i class="fa {{menu.menu_imgurl}}" ng-click="displaymenu()"></i>'+ 
                                 '               <span>{{menu.menu}}</span>'+
                                 '               <span class="pull-right-container">'+
                                 '                      <i class="fa fa-angle-left pull-right"></i>'+
                                 '               </span>'+
                                 '         </a>'+
                                 '           <ul class="treeview-menu ">'+
                                 '           <li ng-repeat="submenu in menu.submenu" class=" treeview"><a ng-click="displaysubmenu(submenu)"><i class="fa fa-spoon"></i> {{submenu.menu_item}}</a></li>'+
                                 '           </ul>'+
                                 '       </li>'+
                                 '</ul>'
                if(!$rootScope.user){
                    return formAdminTemplate;
                }else{
                    return formTemplate;
                }                              
            return formAdminTemplate;
        }
    };

}]);
