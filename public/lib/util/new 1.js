
angular.module('PVM.Util', ['ngTouch', 'ui.grid', 'ui.grid.edit', 'ui.grid.pagination', 'ui.grid.cellNav', 'ui.grid.rowEdit']).directive('vvTable', ['$document', '$rootScope', '$q', '$interval', '$http', '$timeout', function ($document, $rootScope, $q, $interval, $http, $timeout) {
			'use strict';

			return {
				restrict : 'EA',
				scope : {
					vconfig : '=',
					vdata : '=',
					vparent : '=',
					pscope : '=',
					grid:'='
				},
				link : function (scope, element, attrs, ngModel) {
					
					function getData(url) {
						$http({
							method : 'GET',
							url : url
						}).then(function successCallback(response) {
							console.log(response)
							scope.productsGrid.data = response
								scope.vconfig.data = response;
						}, function errorCallback(response) {
							// called asynchronously if an error occurs
							// or server returns response with an error status.
						});
					}
					scope.productsGrid = {
						enableCellEditOnFocus : true,
						// paginationPageSizes: [25,50, 100],
						//paginationPageSize: 25,
						// enableFiltering: true,
						columnDefs : scope.vconfig.columnDefs,
						onRegisterApi : function (gridApi) {
							//set gridApi on scope
							scope.gridApi = gridApi;
							// gridApi.rowEdit.on.saveRow(scope,scope.saveRow);
							gridApi.cellNav.on.navigate(scope, function (newRowcol, oldRowCol) {
								console.log(newRowcol);
							})
							gridApi.edit.on.afterCellEdit(scope, function (rowEntity, colDef, newValue, oldValue) {
								rowEntity.opp = 'E'
									scope.$apply();
							});
							$timeout(function () {
								scope.gridApi.cellNav.scrollToFocus(scope.productsGrid.data[0], scope.productsGrid.columnDefs[1]);
							})

						}
					};
					
					scope.ClickMe = function (fun) {
						scope.vconfig[fun](scope.gridApi.cellNav.getCurrentSelection());
					};
					function generateCols(config) {
						var keys = {}						
						for (var data in config.columnDefs) {
							keys[config.columnDefs[data].name] = ''
						}
						keys.opp = 'N';
						/*if (config.childObj) {
							var childObj=config.childObj
							for (var i = 0; i < childObj.length; i++) { 
								keys[childObj[i].name]=[];
								keys[childObj[i].name].push(generateCols(childObj[i]));
							}
						}*/
						return keys;
					}
					scope.$watch('vdata', function (newData, oldData) {
						if (newData) {
							scope.productsGrid.data = newData;
						}
					}, true);
					scope.$watch('vconfig.data', function (newData, oldData) {
						if (newData) {
							scope.productsGrid.data = newData;
						}
					}, true);
					Array.prototype.removeAt = function (attr, id) {
						for (var item in this) {
							if (this[item][attr] == id) {
								this.splice(item, 1);
								return true;
							}
						}
						return false;
					}

					scope.delete  = function (grid) {
						scope.mySelectedRow = scope.gridApi.cellNav.getCurrentSelection()[0].row.entity.id;
						scope.productsGrid.data.removeAt('id', scope.mySelectedRow)
					}
					scope.add = function (grid) {
						grid.data.push(generateCols(scope.vconfig));
						if (scope.vconfig.childObj) {							
							for (var i = 0; i < scope.vconfig.childObj.length; i++) {
								var childObjGrid=scope.vconfig.childObj[i].grid
								//childObjGrid.data=[];								
								childObjGrid.data.push(generateCols(scope.vconfig.childObj[i]));
							}
						}
						$timeout(function () {
							scope.gridApi.cellNav.scrollToFocus(scope.productsGrid.data[scope.productsGrid.data.length - 1], scope.productsGrid.columnDefs[1]);
						})
					};
					angular.extend(scope.grid, scope.productsGrid);
				},
				template : function (tElem, tAttrs) {
					var template1 = ' <div><div id="productsGrid" ui-grid="productsGrid" ui-grid-edit ui-grid-row-edit  ui-cell-nav ui-grid-cellnav class="grid" ng-style="{width:vconfig.width,height:vconfig.height}"></div></div> <div style="align:right" class="ng-scope"><input type="button" value="Add" ng-click="add(productsGrid)" tabindex="0"><input type="button" value="Delete" ng-click="delete(productsGrid)" tabindex="0"><input type="button" value="Pre"><input type="button" value="Next"></div></div>'
						//var ptemplate='<div class=""><input type="date"    ng-model="seldate"  ng-blur="updatedateDate($event)"> </div>   ';
						return template1;
				}
			};

		}
	]);