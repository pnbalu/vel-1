
angular.module('PVM.Util', ['ngTouch', 'ui.grid', 'ui.grid.edit', 'ui.grid.pagination', 'ui.grid.cellNav', 'ui.grid.rowEdit']).directive('vvTable', ['$document', '$rootScope', '$q', '$interval', '$http', '$timeout', function ($document, $rootScope, $q, $interval, $http, $timeout) {
			'use strict';

			return {
				restrict : 'EA',
				scope : {
					vconfig : '=',
					vdata : '=',
					vparent : '=',
					pscope : '=',
					vgrid:'='
				},
				link : function (scope, element, attrs, ngModel) {
					function init(){
						
					}
					function init(vconfig){
						
						if(vconfig.url){
							$timeout(function () {
							$http({
								method : 'POST',
								url : vconfig.url+"vvdata",
								data:{entity:vconfig.entity.id,fieldlist:getFields(vconfig),filters:vconfig.parent.grid.data[0]?vconfig.parent.id+'='+vconfig.parent.grid.data[0][vconfig.parent.id]:''},
							}).then(function successCallback(response) {
								console.log(response)
								vconfig.grid.data =response.data
								if(vconfig.parent.grid){
									vconfig.parent.grid.data[0][vconfig.name]=response.data;
								}
								//scope.vgrid.data =response.data	
							}, function errorCallback(response) {
								// called asynchronously if an error occurs
								// or server returns response with an error status.
							});
							}, 2000);
						}
						
					}
					function addData(vconfig){
						if(vconfig.parent && vconfig.parent.grid){
							if(vconfig.parent.grid.data[vconfig.parent.grid.data.length-1][vconfig.name]){
								vconfig.parent.grid.data[vconfig.parent.grid.data.length-1][vconfig.name].push(generateCols(vconfig));
							}else{
								vconfig.parent.grid.data[vconfig.parent.grid.data.length-1][vconfig.name]=[];
								vconfig.parent.grid.data[vconfig.parent.grid.data.length-1][vconfig.name].push(generateCols(vconfig));
							}
						}
					}
					function onSelect(child,pvalue,parentData){
						
						if(child.url && !parentData[child.name] && (parentData.opp!='N' && parentData.opp!='E')){
							$timeout(function () {
								$http({
									method : 'POST',
									url : child.url+"vvdata",
									data:{entity:child.entity.id,fieldlist:getFields(child),filters:child.entity.parentid+'='+pvalue},
								}).then(function successCallback(response) {
									console.log(response)
									child.grid.data =response.data
									parentData[child.name] =response.data
									// for child grid's grid
									if(child.childObj && response.data.length!=0){
										$http({
											method : 'POST',
											url : child.childObj[0].url+"vvdata",
											data:{entity:child.childObj[0].entity.id,fieldlist:getFields(child.childObj[0]),filters:child.childObj[0].entity.parentid+'='+parentData[child.name][0][child.childObj[0].parent.id]},
										}).then(function successCallback(response) {
											console.log(response)
											child.childObj[0].grid.data =response.data
											parentData[child.name][0][child.childObj[0].name] =response.data
											// for child grid's grid
												
										}, function errorCallback(response) {
											// called asynchronously if an error occurs
											// or server returns response with an error status.
										});
									}else{
										parentData[child.name][0][child.childObj[0].name]=generateCols(child.childObj[0])
									}
								}, function errorCallback(response) {
									// called asynchronously if an error occurs
									// or server returns response with an error status.
								});
							}, 2000);
						}else{
							if(child.grid){
								child.grid.data=(parentData[child.name]);
							}
							if(child.childObj){								
								if(parentData[child.name]){
									child.childObj[0].grid.data=(parentData[child.name][0][child.childObj[0].name]);
								}
								
								//child.childObj[0].grid.data=onSelect(child.childObj[0],parentData[child.name][0][child.childObj[0].parent.id],parentData[child.name][0]);
								// this is for single 3rd level grid if we need more this should run in for loop
							}						
						}
						
					}
					scope.productsGrid = {rowEditWaitInterval :-1,
						enableCellEditOnFocus : true,
						// paginationPageSizes: [25,50, 100],
						//paginationPageSize: 25,
						// enableFiltering: true,
						columnDefs : scope.vconfig.columnDefs,
						onRegisterApi : function (gridApi) {
							//set gridApi on scope
							scope.gridApi = gridApi;
							// gridApi.rowEdit.on.saveRow(scope,scope.saveRow);
							gridApi.cellNav.on.navigate(scope, function (newRowcol, oldRowCol) {
								//if(scope.vconfig.childObj && oldRowCol!=null){
								if(scope.vconfig.childObj ){
									for (var i=0;i < scope.vconfig.childObj.length;i++) {
										onSelect(scope.vconfig.childObj[i],newRowcol.row.entity[scope.vconfig.childObj[i].parent.id],newRowcol.row.entity);
									}
								}								//console.log(newRowcol);
							})
							gridApi.edit.on.afterCellEdit(scope, function (rowEntity, colDef, newValue, oldValue) {
								if(newValue!=oldValue && rowEntity.opp!='N'){
									rowEntity.opp = 'E'
								}
									scope.$apply();
							});
							$timeout(function () {
								if(scope.vgrid.data){
									scope.gridApi.cellNav.scrollToFocus(scope.vgrid.data[0], scope.productsGrid.columnDefs[1]);
								}
							})

						}
					};
					//init(scope.vconfig);
					angular.merge(scope.vgrid, scope.productsGrid);
					scope.ClickMe = function (fun) {
						scope.vconfig[fun](scope.gridApi.cellNav.getCurrentSelection());
					};
					
					function getFields(config) {
						var keys = "";
						for (var i=0;i < config.columnDefs.length;i++) {
							keys=keys+""+config.columnDefs[i].name+","							
						}
						keys=keys.substring(0,keys.length-1)
						return keys;
					}
					function generateCols(config) {
						var keys = {}
						for (var data in config.columnDefs) {
							keys[config.columnDefs[data].name] = ''
						}
						if(config.childObj){
							for (var i = 0; i < config.childObj.length; i++) {
								keys[config.childObj[i].name]=[]
							}
						}
						keys.opp = 'N';
						/*if (config.childObj) {
							var childObj=config.childObj
							for (var i = 0; i < childObj.length; i++) {
								keys[childObj[i].name]=[];
								keys[childObj[i].name].push(generateCols(childObj[i]));
							}
						}*/
						return keys;
					}
					// Create a dats model with child and parent relation ship
					function createDataModel(config){
						var pdata= config.grid.data;
						for (var i = 0; i < config.gridlength; i++) {

						}
					}
					scope.$watch('vdata', function (newData, oldData) {
						if (newData) {
							//scope.productsGrid.data = newData;
							scope.vgrid.data = newData;
						}
					}, true);
					scope.$watch('vconfig.data', function (newData, oldData) {
						if (newData) {
							//scope.productsGrid.data = newData;
							scope.vgrid.data = newData;
						}
					}, true);
					Array.prototype.removeAt = function (attr, id) {
						for (var item in this) {
							if (this[item][attr] == id) {
								this.splice(item, 1);
								return true;
							}
						}
						return false;
					}

					scope.delete  = function (grid) {
						scope.mySelectedRow = scope.gridApi.cellNav.getCurrentSelection()[0].row.entity[scope.vconfig.entity.primary];
						scope.vgrid.data.removeAt(scope.vconfig.entity.primary, scope.mySelectedRow)
						var where =scope.vconfig.entity.primary+"="+scope.mySelectedRow
						if(scope.vconfig.isSingle){
							$http({
							method: 'POST',
							url: 'http://localhost:3000/vvdelete',
							data:{entity:scope.vconfig.entity.id,where:where}	
							}).then(function successCallback(response) {               
								//$scope.menuData = response.data;
							}, function errorCallback(response) {

							});	
						}
					}
					scope.save  = function (grid) {
						scope.mySelectedRow = scope.gridApi.cellNav.getCurrentSelection()[0].row.entity[scope.vconfig.entity.primary];
						$http({
							method: 'POST',
							url: 'http://localhost:3000/vvsavel',
							data:{data:scope.vconfig.grid.data,config:{entity:scope.vconfig.entity.id}}	
						}).then(function successCallback(response) {               
							//$scope.menuData = response.data;
						}, function errorCallback(response) {

						});	
					}
					function getChildGrid(){
						if (scope.vconfig.childObj) {
							for (var i = 0; i < scope.vconfig.childObj.length; i++) {
								var childObjGrid=scope.vconfig.childObj[i].grid
								childObjGrid.data=[];
								childObjGrid.data.push(generateCols(scope.vconfig.childObj[i]));
							}
						}
					}
					scope.add = function (currentObj) {
						if(currentObj.grid.data){
							currentObj.grid.data.push(generateCols(scope.vconfig));						
						}else{
							currentObj.grid.data=[];
							currentObj.grid.data.push(generateCols(scope.vconfig));
						}
						if(currentObj.childObj ){
							for (var i = 0; i < currentObj.childObj.length; i++) {
								if(currentObj.grid.data[currentObj.grid.data.length-1][currentObj.childObj[i].name].length==0 ){
									currentObj.childObj[i].grid.data=[];
								}
							}
						}
						//addData(currentObj);
						/*if (scope.vconfig.childObj) {
							for (var i = 0; i < scope.vconfig.childObj.length; i++) {
								var childObjGrid=scope.vconfig.childObj[i].grid
								if(childObjGrid.data){
									childObjGrid.data=[];
								}								
								childObjGrid.data.push(generateCols(scope.vconfig.childObj[i]));
							}
						}*/
						$timeout(function () {
							scope.gridApi.cellNav.scrollToFocus(scope.vgrid.data[scope.vgrid.data.length - 1], scope.vgrid.columnDefs[1]);
						})
					};

				},
				template : function (tElem, tAttrs) {
					var template1 = ' <div class="vv-grid">'+
									' <div id="vgrid" ui-grid="vgrid" ui-grid-edit ui-grid-row-edit  ui-cell-nav ui-grid-cellnav class="grid" ng-style="{width:vconfig.width,height:vconfig.height}"></div>'+
									' <div style="align:right" class="ng-scope">'+
									'	<button class="btn" type="button" value="Add" ng-click="add(vconfig)" tabindex="0">Add</button>'+
									'	<button class="btn" type="button" value="Delete" ng-click="delete(vgrid)" tabindex="0">Delete</button>'+
									'	<button class="btn" ng-show="vconfig.isSingle"type="button" value="Add" ng-click="save(vconfig)" tabindex="0">save</button>'+
									'	<button class="btn" ng-show="vconfig.isPreNext" type="button" value="Pre">Pre</button>'+
									'	<button class="btn" ng-show="vconfig.isPreNext" type="button" value="Next">Next</button>'+
									'</div>'+
									'</div>'
				
						return template1;
				}
			};

		}
	]);