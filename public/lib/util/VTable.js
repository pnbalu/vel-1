
angular.module('PVM.Util',['xeditable']).directive('vTable', ['$document','$rootScope', function($document,$rootScope) {
    'use strict';

    var setScopeValues = function (scope, attrs) {
        scope.format = attrs.format || 'YYYY-MM-DD';
       // scope.viewFormat = attrs.viewFormat || 'Do MMMM YYYY';
        scope.viewFormat = Environment.userInfo.dateFormat;
        scope.locale = attrs.locale || 'en';
        scope.firstWeekDaySunday = scope.$eval(attrs.firstWeekDaySunday) || false; 
        scope.placeholder = attrs.placeholder || '';
        scope.years=attrs.years || '1900-2030';

        scope.position=attrs.position||'fixed';
        scope.mobileInputWidth = $rootScope.isDeviceMobile() ? '90%' : '100%';
    };



    return {
        restrict: 'EA',
        require: '?ngModel',
        scope: { ngModel: '='},
        link: function (scope, element, attrs, ngModel) {
			var vt={theaders:['name','TokenStart'],tconfig:{name:{type:'text'},token:{type:'text'},age:{type:'text'}},tdata:[{name:'balu',token:'12345',age:'12345'},{name:'balu1',token:'12345678',age:'12345'}]}
				
			var ctable=[];
			ctable.theaders=vt.theaders;			
			for(var j=0;j<vt.tdata.length;j++){
				var row={};
				for(var data in vt.tdata[j]){
					if(vt.tconfig[data]){
						row[data]={};							
						row[data]['value']=vt.tdata[j][data];
						row[data]['config']=vt.tconfig[data]
					}					
				}
				ctable.push(row)
			}
			console.log(ctable)
			scope.addRow=function(){
				var row={}
				for(var data in vt.tconfig){
					vt.tconfig[data]['value']='-';
				}
				ctable.push(vt.tconfig)
			}
			scope.ct=ctable;
           /* ngModel.$render = function () {
				var newValue = ngModel.$viewValue;
				if (newValue !== undefined && newValue !== '' && newValue !== null) {
					var defaultDate = moment(newValue,scope.format);
					scope.viewValue = defaultDate.format(scope.viewFormat);                    
					//scope.viewValue = moment(newValue).format(attrs.viewFormat);
					scope.dateValue = newValue;
				}
            };*/

        },
        template:function(tElem, tAttrs){
                var template1= ' <div >'+
					' <table class="table table-bordered table-hover table-condensed">'+
					'	<tr style="font-weight: bold" >'+
					'	  <td style="width:35%" ng-repeat="theader in ct.theaders">{{theader}}</td>'+
					'	</tr>'+
					'	<tr ng-repeat="alldata in ct" e-form="rowform">'+
					'	  <td ng-repeat="data in alldata">'+
					'		<span editable-text="user.name" ng-click="rowform.$show()" e-name="name" e-form="rowform" onbeforesave="checkName($data, user.id)" e-required>'+
					'		  {{ data.value}}'+
					'		</span>'+
					'	  </td><td><form editable-form name="rowform"></form></td>'+						  
					'	</tr>'+
					'  </table>'+
					'  <button type="button" class="btn btn-default" ng-click="addRow()">Add</button>'+
					'</div>'
            //var ptemplate='<div class=""><input type="date"    ng-model="seldate"  ng-blur="updatedateDate($event)"> </div>   ';
            return template1;
        }
    };

}]);

