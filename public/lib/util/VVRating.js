app.directive('vvRatings', ['$document','$rootScope', function($document,$rootScope) {
    'use strict';

   
    return {
      restrict: 'EA',
      template:
        '<table class="star-rating form-group" ng-class="{readonly: readonly}"><tr>' +
        '<td style="    width: 66px;padding-left: 5px;">{{label}}</td>  <td ng-repeat="star in stars" class="star" ng-class="{filled: star.filled}" ng-click="toggle($index)">' +
        '    <i class="fa fa-star"></i>' + // or &#9733
        '  </td>' +
        ' <td>&nbsp;&nbsp;&nbsp;</td><td> <span class="label label-w1arning yellow-s-button" ng-click="remove()"><i class="fa fa-times" ></i></span></td>'+
        '</table>',
      scope: {
        ratingValue: '=ngModel',
        max: '=?', // optional (default is 5)
        label:"=",
        onRatingSelect: '&?',
        readonly: '=?'
      },
      link: function(scope, element, attributes) {
       
        if (scope.max == undefined) {
          scope.max = 5;
        }
        function updateStars() {
          scope.stars = [];
          for (var i = 0; i < scope.max; i++) {
            scope.stars.push({
              filled: i < scope.ratingValue
            });
          }
        };
        scope.remove=function(){
            scope.ratingValue =0;
        }
        scope.toggle = function(index) {
          if (scope.readonly == undefined || scope.readonly === false){
            scope.ratingValue = index + 1;
            //scope.onRatingSelect({
            //  rating: index + 1
            //});
          }
        };
        scope.$watch('ratingValue', function(oldValue, newValue) {
          if (newValue) {
            updateStars();
          }
        });
      }
    };

}]);
