angular.module("PVM.Util2", []).directive("vvLov", ['$modal','$http','$rootScope', function ($modal,$http,$rootScope) {
			return {
				restrict : 'A',
				scope : {
					model : '=',
					title : '=',
					callback : '&',
					columnlist : '=',
					fmodel : '=',
					fieldlist : '=',
					config : '='
				},
				link: function (scope, element, attrs) {
					
				},
				template : '<input type="text" class="btn btn-default" ' +
				'ng-click="openPopUp(config)" ng-model="fmodel">' +
				' <span class="glyphicon glyphicon-search"></span>' +
				'</button>',
				controller : ['$scope', '$element', function ($scope, $element) {
						
						$scope.openPopUp = function (config) {
							if(!config){
								config=$scope.$parent.col.colDef.lovConfig
							}
							var columnlist=[];
							var fieldlist=[];
							var fmodel=config.fmodel;
							var callback=config.callback;
							var title=config.title
							var model=config.data;													
							var serLOV={};
							var setKeys="";
													
							for (i = 0; i < config.colNames.length; i++) { 
								for ( property in config.colNames[i] ) {
								  columnlist.push(config.colNames[i][property]);
								  setKeys=setKeys+""+property+","
								  fieldlist.push(property);								  
								}
							}
							setKeys=setKeys.substring(0,setKeys.length-1)
							serLOV.entity=config.entity	
							serLOV.fieldlist=setKeys;													
							$scope.fmodel=fmodel;
							$scope.localPopUp = {
								backdrop : true,
								backdropClick : true,
								dialogFade : false,
								keyboard : true,
								template : '<div class="modal-header">' +
								'    <div class="row">' +
								'        <div class="col-sm-6">' +
								'            <div class="text-primary">' +
								'                <h3>' +
								'                    {{Title}}' +
								'                </h3>' +
								'            </div>' +
								'        </div>' +
								'        <div class="col-sm-6">' +
								'           <button type="button" class="close" ng-click="CloseLov()" aria-hidden="true">&times;</button>' +
								'        </div>' +
								'    </div>' +
								'</div>' +
								'<div class="modal-body" style="height:380px">' +
								'    <div class="row">' +
								'        <div class="col-sm-12">' +
								'            <input type="text"' +
								'                   autocomplete="off"' +
								'                   id="txtFilter"' +
								'                   ng-model="txtFilter"' +
								'                   class="form-control input-lg" autofocus />' +
								'        </div>' +
								'    </div>' +
								'    <p></p>' +
								'    <table class="table table-hover table-striped">' +
								'        <tr>' +
								'            <th ng-repeat="c in ColumnList">{{c}}</th>' +
								'            <th></th>' +
								'        </tr>' +
								'        <tr ng-repeat="x in Model | filter: txtFilter   | StartOnPage: CurrentPage*PageSize | limitTo:PageSize"  ng-click="ChooseItem(x)">' +
								'            <td ng-repeat="c in FieldList">' +
								'                {{x[c]}}' +
								'            </td>' +
								'        </tr>' +
								'    </table>' +
								'</div>' +
								'<div class="modal-footer">' +
								'    <ul class="pagination">' +
								'        <li ng-class="{disabled: CurrentPage == 0}">' +
								'            <a href ng-click="PrevPage()">« Prev</a>' +
								'        </li>' +
								'        <li><a href>Current Page {{CurrentPage+1}}</a></li>' +
								'        <li><a href>/</a></li>' +
								'        <li><a href>Total Pages {{TotalPages()}}</a></li>' +
								'        <li ng-class="{disabled: CurrentPage+1 == TotalPages()}">' +
								'            <a href ng-click="NextPage()">Next »</a>' +
								'        </li>' +
								'    </ul>' +
								'</div>',
								controller : 'LovCtrl',
								windowClass : '',
								resolve : {
									Model : function () {
										return model;
									},
									Title : function () {
										return title;
									},
									ColumnList : function () {
										return columnlist;
									},
									FieldList : function () {
										return fieldlist;
									},
									FModel : function () {
										return fmodel;
									}
								}
							};
	
							var modalInstance;// = $modal.open($scope.localPopUp);
							$http({
							  method: 'POST',
							  url: 'http://localhost:3000/lov',
							  data:serLOV
							}).then(function successCallback(response) {               
								model = response.data;
								modalInstance = $modal.open($scope.localPopUp);
								modalInstance.result.then(function (selectedItem) {
									$scope.fmodel=selectedItem[config.fmodel];
								callback({
									e : selectedItem
								});
								}, function () {
									//dismiss: nothing to do
								});
							}, function errorCallback(response) {
							   
							});	
							
						}
					}
				]
			}
		}
	])
.filter('StartOnPage', function () {
	return function (input, start) {
		start = +start;
		return input.slice(start);
	}
})
.controller('LovCtrl', ['$scope', '$log', '$rootScope', '$modalInstance', 'Model', 'Title', 'ColumnList', 'FieldList','FModel',
		function ($scope, $log, $rootScope, $modalInstance, Model, Title, ColumnList, FieldList,FModel) {

			$scope.ColumnList = ColumnList; //list of columns to show
			$scope.FieldList = FieldList; //list of fields to show
			$scope.Model = Model; //data we want to show
			$scope.Title = Title; //title for the modal

			$scope.PageSize = 5; //max records per page
			$scope.CurrentPage = 0; //initial page

			//how many pages are available
			$scope.TotalPages = function () {
				return Math.ceil($scope.Model.length / $scope.PageSize);
			}

			//moves back
			$scope.PrevPage = function () {
				if ($scope.CurrentPage > 0)
					$scope.CurrentPage--;
			}

			//moves forward
			$scope.NextPage = function () {
				if ($scope.CurrentPage < $scope.TotalPages() - 1)
					$scope.CurrentPage++;
			}

			//when you click "choose" button
			$scope.ChooseItem = function (x) {
				FModel=x.role;
				$modalInstance.close(x);
			}

			//when you clic "X" button
			$scope.CloseLov = function () {
				$modalInstance.dismiss('cancel');
			};
		}
	]);