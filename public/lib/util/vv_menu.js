angular.module('PVM.Util4',[]).directive('vvMenu', ['$document','$rootScope','$http','HmmService', function($document,$rootScope,$http,HmmService) {
    'use strict';   
    return {
       restrict: 'EA',
        scope: { vconfig: '=',vdata:'='},
        link: function (scope, element, attrs, ngModel) {			
            function convertContent(obj){
                for (var i = 0; i < obj.length; i++) {
                    if(obj[i].ingrediants){                        
                        //console.log(obj[i]);
                        obj[i].ingrediants=JSON.parse(obj[i].ingrediants);
                    }   
                }
                //console.log(obj)
                return obj;
            }
            function contructMenu(mdata,parent){
                var menus = mdata;              
                for (var i = 0; i < menus.length; i++) {                  
                    
                    var menu=menus[i].menu;
                    var tempmenu;

                    var sub_menu=menus[i].menu_item;
                    var sub_tempmenu;

                    if(tempmenu != menu){ 
                        var p_menus={};                           
                        p_menus.menu=menu;
                        p_menus.submenu=[];
                        parent.push(p_menus); 
                        tempmenu=menu                                                                               
                    }

                    if(sub_tempmenu != sub_menu){ 
                        p_menus.submenu.push(menus[i]);
                        sub_tempmenu=sub_menu                                                        
                    }                        
                    //console.log(parent);
                }
                return parent
            }

	            var url = "";
                var role=3
                // scope.vv_save=function(){
            	//console.log($rootScope.user);
                if(!$rootScope.user){
                    role=3
                }else{
                   role=$rootScope.user.role 
                }
				$http({
					method: 'POST',
					url: '/getkmenu',
					data:{branch_id:HmmService.getBranchId()}
				}).then(function successCallback(response) {               
					var parent=[]
                    scope.parent=contructMenu(convertContent(response.data),parent)
                    $rootScope.pMenu=response.data
				}, function errorCallback(response) {

				});	 
                
                 scope.displaysubmenu=function(menu){
                    $rootScope.selectedfood=menu
                }
                scope.searchFood=function(menu){
                    $rootScope.selectedfood=scope.searchMenu
                }
        },
        template:function(tElem, tAttrs){               
                var formTemplate = '<ul class="sidebar-menu">'+
                                '       <li class="active treeview" ng-repeat="menu in menus">'+
                                 '         <a href="#/{{menu.menu_url}}">'+
                                 '           <i class="fa {{menu.menu_imgurl}}"></i> <span>{{menu.menu_name}}</span>'+
                                 '         </a>'+
                                 '       </li>'
                                 '</ul>';

                var formAdminTemplate = 
                                 '   <form action="#" method="get" class="sidebar-form">'+
                                 '       <div class="input-group">'+
                                 '           <input type="text" ng-model="searchMenu" name="q" ng-keyup="searchFood()"class="form-control" placeholder="Search Food...">'+
                                 '           <span class="input-group-btn">'+
                                 '                <button type="submit" name="search" id="search-btn" class="bt1n btn-fl1at search-btn"><i class="fa fa-search"></i>'+
                                 '           </button>'+
                                 '           </span>'+
                                 '       </div>'+
                                
                                 '   </form>'+
                                 '<ul class="sidebar-menu">'+
                                 '       <li class=" treeview active" ng-repeat="menu in parent">'+
                                 '         <a class="fa  fa-cutlery " href="void">'+
                                 '           <i class="fa {{menu.menu_imgurl}}" ng-click="displaymenu()"></i>'+ 
                                 '               <span>{{menu.menu}}</span>'+
                                 '               <span class="pull-right-container">'+
                                 '                      <i class="fa fa-angle-left pull-right"></i>'+
                                 '               </span>'+
                                 '         </a>'+
                                 '           <ul class="treeview-menu ">'+
                                 '           <li ng-repeat="submenu in menu.submenu" class=" treeview"><a href="#/food" ng-click="displaysubmenu(submenu.menu_item)"><i class="fa fa-spoon"></i> {{submenu.menu_item}}</a></li>'+
                                 '           </ul>'+
                                 '       </li>'+
                                 '</ul>'
                if(!$rootScope.user){
                    return formAdminTemplate;
                }else{
                    return formTemplate;
                }                              
            return formAdminTemplate;
        }
    };

}]);
