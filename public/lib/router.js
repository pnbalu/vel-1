angular.module('PVM', ['ngMaterial','ngDialog','ang.datepicker','chart.js','angucomplete','ngRoute','UserService','PVM.controllers','PVM.Util','PVM.Util2','PVM.Util3','PVM.Util4','PVM.Utils','pvm_hmm','dynform','ui.bootstrap','pvm_hmm_management','pvm_hmm_order'])
.config(function($routeProvider) {
    $routeProvider.
      when('/dtoken', {controller:'seachUserCtrl', templateUrl:'dtoken.html'}).
      when('/token', {controller:'seachUserCtrl', templateUrl:'token.html'}).
      when('/addclinic', {controller:'seachUserCtrl', templateUrl:'addclinic.html'}).
      when('/register/', {controller:'seachUserCtrl', templateUrl:'register.html'}).
      when('/trackorder/', {controller:'seachUserCtrl', templateUrl:'trackorder.html'}).
      when('/user/', {controller:'seachUserCtrl', templateUrl:'userlist.html'}).
      when('/userInfo/', {controller:'userInfoCtrl', templateUrl:'userlist.html'}).
      when('/user/edit/:userId', {controller:'editUserCtrl', templateUrl:'userdetail.html'}).
      when('/user/new', {controller:'newUserCtrl', templateUrl:'userdetail.html'}).
	    when('/', {controller:'seachUserCtrl', templateUrl:'login.html'}).
	    when('/fpassword', {controller:'seachUserCtrl', templateUrl:'fpassword.html'}).
      when('/cpassword', {controller:'seachUserCtrl', templateUrl:'cpassword.html'}).
      when('/admin', {controller:'menuCtrl', templateUrl:'vv-fm/admin.html'}).
      when('/edit/:projectId', {controller:'editProjectCtrl', templateUrl:'detail.html'}).
      when('/new', {controller:'newProjectCtrl', templateUrl:'detail.html'}).
      when('/home', {controller:'homeCtrl', templateUrl:'home.html'}).
      otherwise({redirectTo:'/'});
  });