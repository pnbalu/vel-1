'use strict';
angular.module('PVM.controllers', [])
.controller('seachUserCtrl', ['$q', '$http', '$scope', '$rootScope', '$location', '$routeParams', '$window','User','FmService','HmmService',
	function ($q, $http, $scope, $rootScope, $location, $routeParams, $window,User,FmService,HmmService) {
        $scope.formData = {};
        $scope.formTemplate = [
		{
            "type": "text",
            "label": "Name",
            "model": "user.name"
        },
        {
            "type": "text",
            "label": "User Id( E-mail )",
            "model": "user.email"
        },
        {
            "type": "password",
            "label": "Password",
            "model": "user.password"
        },
		{
            "type": "password",
            "label": "Re Type Password",
            "model": "user.password"
        },
        {
            "type": "number",
            "label": "Phone Number",
            "model": "user.phone"
        },
        {
            "type": "submit",
			"label":"Save",
            "model": "submit",
			"callback":"save()"
        },
    ];
        $scope.user={};
		//$scope.users = User.query();
		//$scope.user={email:'tnbalu@yahoo.com',password:'2'}
		//cope.user.password='rrrrrrrrrrrr'
		$scope.isLoggedIn=false;
		$scope.company=HmmService.getLocData('branches')[0];
		$scope.user.iscolsed=false;
		$scope.login = function() {
			
			User.get({id: $scope.user.email,password:$scope.user.password}, function(user) {
				self.original = user;
				$scope.user = user;//new User(self.original);
				if(user.email){
					FmService.setUser(user) 
					$scope.role=user.role;    
					$scope.loggedin=true;
					$scope.user=angular.copy($scope.user);
					$scope.user_role=user.role; 
					$scope.user.iscolsed=false
					$scope.isLoggedIn=true;
					if(user.role="HAdmin"){
						$location.path('/order');
					}else{				
						$location.path('/food');
					}
				}else{
					$location.path('/');
				}


			});
		};
			$scope.saveUser = function() {
				$http({
					method: 'POST',
					data:{user:$scope.user},
				  url: '/saveuser'
				}).then(function successCallback(response) {               
					$scope.userData = response.data;
					if(response.data.error){
						FmService.dialogFail('Your Registration Failed contact Administrator ' +response.data.error)
					}else{
					 FmService.dialogPass('Your have registered successfully, Please login to to your e-mail , verify and Login'); 
					 $location.path('/food');   					
					}
				}, function errorCallback(response) {
				   FmService.dialogFail('Your Registration Failed contact Administrator')
				});
				$scope.user.sofpassword=false;
					$scope.user.sofpassword=false;
					$scope.user.iscolsed=false

		  };
		  $scope.forgotPassword = function() {
				$http({
					method: 'POST',
					data:{user:$scope.user},
				  url: '/fgPassword'
				}).then(function successCallback(response) {               
					$scope.userData = response.data;
					 FmService.dialogPass('Your password sent to your email address'); 
				}, function errorCallback(response) {
				   
				});
				$scope.user.sofpassword=false;
					$scope.user.iscolsed=false

		  };
		   $scope.changePassword = function() {
				$http({
					method: 'POST',
					data:{user:$scope.user},
				  url: '/chgPassword'
				}).then(function successCallback(response) { 
				    FmService.dialogPass('Your password changed successfully');            
					
				}, function errorCallback(response) {
				   FmService.dialogFail('Your password not changed contact administrator');      
				});
				$scope.userData = response.data;
					$scope.user.sofpassword=false;
					$scope.user.sofpassword=false;
					$scope.user.iscolsed=false
		  };


		  $rootScope.showCart=function(){
	            $rootScope.leftmenu
	          if( $rootScope.enablecart){
	             $rootScope.enablecart=false
	          }else{
	             $rootScope.enablecart=true
	          }
        }  
	}
])
.controller('menuCtrl', ['$q', '$http', '$scope', '$rootScope', '$location', '$routeParams', '$window','User',
	function ($q, $http, $scope, $rootScope, $location, $routeParams, $window,User) {
		 var self = this;
		 $scope.menuGrid={};
		 $scope.menuSaveConfig={url:'/savemenu',entity:'vv_menu'}

		 $scope.menuConfig = {  name:'menu',entity:{id:'vv_menu',primary:'id'}, grid:$scope.menuGrid,width:'900px',height:'200px',isSingle:true,
				columnDefs: [
				{ name: 'id', visible:false},
				{ name: 'menu_name', displayName: 'Name',visible:true},
				{ name: 'menu_url', displayName: 'URL',visible:true},
				{ name: 'menu_submenu', displayName: 'Sub Menu',visible:true }, 
				{ name: 'menu_order', displayName: 'Menu Order',visible:true }, 
				{ name: 'menu_imgurl', displayName: 'Image URL' ,visible:true}								
		]};
		
		$http({
            method: 'POST',
          	url: '/getmenu',
		  	data:{entity:'vv_menu'}	
        }).then(function successCallback(response) {               
            $scope.menuData = response.data;
        }, function errorCallback(response) {
           
        });
         $scope.roleGrid={};
		 $scope.roleConfig = {  name:'role',entity:{id:'vv_role',primary:'id'}, grid:$scope.roleGrid,width:'300px',height:'200px',isSingle:true,
				columnDefs: [
				{ name: 'id', visible:false},
				{ name: 'role_name', displayName: 'Name',visible:true}				
		]};
		
		$http({
            method: 'POST',
          	url: '/vvdata',
		  	data:{entity:'vv_role'}	
        }).then(function successCallback(response) {               
            $scope.roleData = response.data;
        }, function errorCallback(response) {
           
        });
        $scope.entityGrid = {};
        $scope.entityConfig = {  name:'entity',entity:{id:'user_entity',primary:'usr_ent_id'}, grid:$scope.entityGrid, width:'800px',height:'200px',isSingle:true,
				columnDefs: [
				{ name: 'usr_ent_id', visible:false},
				{ name: 'usr_ent_name', displayName: 'Name',visible:true},
			//	{ name: 'usr_role_name', displayName: 'Role',visible:true,lovConfig:$scope.roleLOVConfig,cellTemplate:"<div vv-lov  config='roleLOVConfig'  ></div>"},
				{ name: 'usr_ent_address1', displayName: 'Address1',visible:true }, 
				{ name: 'usr_ent_address2', displayName: 'Address2' ,visible:true},
				{ name: 'usr_ent_city', displayName: 'City' ,visible:true},
				{ name: 'usr_ent_state', displayName: 'State' ,visible:true},
				{ name: 'usr_ent_zip', displayName: 'Zip' ,visible:true}				
			]};
			
			$http({
                method: 'POST',
              url: '/vvdata',
			  data:{entity:'user_entity'}	
            }).then(function successCallback(response) {               
                $scope.entityData = response.data;
            }, function errorCallback(response) {
               
            });     
		  
	}
])
.controller('profileCtrl', ['$q', '$http', '$scope', '$rootScope', '$location', '$routeParams', '$window','User',
	function ($q, $http, $scope, $rootScope, $location, $routeParams, $window,User) {
        //TODO
	}
])
.controller('editUserCtrl', ['$q', '$http', '$scope', '$rootScope', '$location', '$routeParams', '$window','User',
	function ($q, $http, $scope, $rootScope, $location, $routeParams, $window,User) {
		 var self = this;

		  User.get({id: $routeParams.userId}, function(user) {
			self.original = user;
			$scope.user = new User(self.original);
		  });

		  $scope.isClean = function() {
			return angular.equals(self.original, $scope.user);
		  }
		
			
		  $scope.destroy = function() {
			self.original.destroy(function() {
			  $location.path('/user/list');
			});
		  };

		  $scope.save = function() {
			$scope.user.update(function() {
			  $location.path('/user/');
			});
		  };		
	}
])
.controller('userInfoCtrl', ['$q', '$http', '$scope', '$rootScope', '$location', '$routeParams', '$window','User',
	function ($q, $http, $scope, $rootScope, $location, $routeParams, $window,User) {
		 var self = this;
        $scope.showMe = function(obj){
            alert('Success');
            console.log(obj)
        };
			$scope.profile={};
			$scope.profileData = {};
			$scope.userGrid = {};
			$scope.entityGrid = {};

			 $scope.roleGrid={};
			 $scope.roleConfig = {  name:'role',entity:{id:'role',primary:'id'}, grid:$scope.roleGrid,width:'300px',height:'200px',isSingle:true,parent:{id:'usr_ent_id',grid:$scope.entityGrid},
					columnDefs: [
					{ name: 'id', visible:false},
					{ name: 'role_name', displayName: 'Name',visible:true}				
			]};
			$scope.roleLOVConfig={dmodel:'usr_role_name',fmodel:'role_name',title:'Role',entity:'role',colNames:[{role_name:'Role',id:'ID'}]}
			$scope.userConfig = { name:'user',entity:{id:'users',primary:'id'},parent:{id:'usr_ent_id',grid:$scope.entityGrid},url:'/',grid:$scope.userGrid,vfunction:$scope.showMe,width:'900px',height:'200px',columnDefs: [
                    { name: 'id', visible:false},
                    { name: 'name', displayName: 'Name',visible:true},
                    { name: 'email', displayName: 'E-Mail',visible:true},
                    { name: 'password', displayName: 'Password' }, 
                 //   { name: 'role', displayName: 'Role',visible:true,lovConfig:$scope.roleLOVConfig,cellTemplate:"<div vv-lov  config='roleLOVConfig'  ></div>" }, 
                    { name: 'phonenumber', displayName: 'Phone number' },
                    { name: 'Invite', displayName: 'Invite', width: '10%' ,cellTemplate:'<button class="btn primary" ng-click="grid.appScope.ClickMe(\'vfunction\')">Invite</button>',cellEditableCondition:false}             
                ]};
				
				$scope.fmodel="TEst"
			
			$scope.profileTemplate = [
				{
					"type": "text",
					"label": "Name",
					"model": "profile.name"
				},
				{
					"type": "checkbox",
					"label": "isActive",
					"model": "profile.isActive"
				},
				{
					"type": "text",
					"label": "Type",
					"model": "profile.type"
				}
			];
			
			
			
           /* $http({
                method: 'POST',
              url: '/user'
            }).then(function successCallback(response) {               
                $scope.userData = response.data;
            }, function errorCallback(response) {
               
            });*/
			

		  $scope.isClean = function() {
			return angular.equals(self.original, $scope.user);
		  }
		
			
		  $scope.destroy = function() {
			self.original.destroy(function() {
			  $location.path('/user/list');
			});
		  };

		  $scope.save = function() {
			$scope.user.update(function() {
			  $location.path('/user/');
			});
		  };		
	}
])
.controller('homeCtrl', ['$q', '$http', '$scope', '$rootScope', '$location', '$routeParams', '$window','User',
	function ($q, $http, $scope, $rootScope, $location, $routeParams, $window,User) {
		 var self = this;

        
		  $scope.isClean = function() {
			return angular.equals(self.original, $scope.user);
		  }
		
			
		  $scope.destroy = function() {
			self.original.destroy(function() {
			  $location.path('/user/list');
			});
		  };

		  $scope.save = function() {
			$scope.user.update(function() {
			  $location.path('/user/');
			});
		  };		
	}
])
.controller('newUserCtrl', ['$q', '$http', '$scope', '$rootScope', '$location', '$routeParams', '$window','User',
	function ($q, $http, $scope, $rootScope, $location, $routeParams, $window,User) {
		 $scope.save = function() {
			User.save($scope.user, function(user) {
			  $location.path('/user/edit/' + user.id);
			});
		  }		
	}
])
