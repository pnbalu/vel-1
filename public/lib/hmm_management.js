angular.module('pvm_hmm_management', ['xeditable'])
    .config(function($routeProvider) {
        $routeProvider.
            when('/foodmgt', {controller:FoodChgCtrl, templateUrl:'hmm/hmm_food_management.html'}).  
            when('/branchmgt', {controller:BranchMgtCtrl, templateUrl:'hmm/hmm_branch_management.html'}).
            when('/graphmgt', {controller:GraphMgtCtrl, templateUrl:'hmm/hmm_graph_management.html'}).             
            otherwise({redirectTo:'/'});
  })

function BranchMgtCtrl($scope,$http, User,$rootScope,$location,HmmService,FmService) {  
           $scope.user=HmmService.getData('user');
    $scope.role=$scope.user.role;
    $scope.company=HmmService.getData('branch');
        $http({       
        url: '/get_branch',
        method: 'post',
        data:{filters:'com.company_id='+JSON.parse(HmmService.getData('company') )}
        }).then(function successCallback(response) {          
            $scope.branchess = response.data;         
        }, function errorCallback(response) {
           
        }); 
        
        $scope.updateXeditBranch=function(branch,param){
            $http({
                method: 'POST',
                url: '/save_branch',
                data:{branch:branch,param:param} 
            }).then(function successCallback(response) {               
              //FmService.success(response)
            }, function errorCallback(response) {
               
            }); 
        } 
     
}
function FoodChgCtrl($scope,$http, FmService,HmmService) {  
        $scope.user=HmmService.getData('user');
    $scope.role=$scope.user.role;
    $scope.company=HmmService.getData('branch');
    var search={branch:1}
    $http({
        method: 'POST',
        url: '/get_food',
        data:{search:search} 
        }).then(function successCallback(response) {               
            $scope.admfood = response.data;
        }, function errorCallback(response) {
           
        });

     $scope.searchFood=function(search){
        search.branch=1;
        $http({
        method: 'POST',
        url: '/get_food',
        data:{search:search} 
        }).then(function successCallback(response) {               
          $scope.admfood = response.data;
        }, function errorCallback(response) {
           
        }); 
    }

    var ffood={}
    $scope.updateXeditFood=function(food,param){
        $http({
        method: 'POST',
        url: '/save_food',
        data:{food:food,param:param} 
        }).then(function successCallback(response) {               
          FmService.success(response)
        }, function errorCallback(response) {
           
        }); 
    }
        
}
function GraphMgtCtrl($scope,$http, $rootScope,$location,HmmService,FmService) {  
    $scope.user=HmmService.getData('user');
    $scope.role=$scope.user.role;
        $scope.branches=HmmService.getLocData('branches');
        $scope.periods=['Day','Month','Week','Year'];
        $scope.gdata={}
        $scope.company=HmmService.getData('branch');
        $scope.search={from:new Date(new Date().setFullYear(new Date().getFullYear() - 1)),to:new Date()}
        $scope.draw=function(search){
            draw(search);
        }
        function draw(){
            
            var torder ={company:JSON.parse(HmmService.getData('company')),search:$scope.search}
            $http({       
            url: '/graph_order',
            method: 'post',
            data:{order:torder}
            }).then(function successCallback(response) {          
                $scope.rev_graph = response.data;
                var price=[];
                var tips=[];
                var branch=[];
                var priod=[]  
                for (var i = 0; i < $scope.rev_graph .length; i++) {
                    price.push($scope.rev_graph[i].price);
                    tips.push($scope.rev_graph[i].tips);
                    branch.push($scope.rev_graph[i].branch);
                    priod.push($scope.rev_graph[i].period);
                } 
                $scope.tips =  tips;
                $scope.labels =  priod;
                $scope.data =  price;    
            }, function errorCallback(response) {
               
            }); 
        }
        

         // $scope.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
         // $scope.series = ['Series A', 'Series B'];

         // $scope.data = [65, 59, 80, 81, 56, 55, 20];
     
}
