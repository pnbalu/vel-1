angular.module('pvm_hmm', ['UserService'])
	.config(function($routeProvider) {
		$routeProvider.
			when('/stock', {controller:ListCtrl, templateUrl:'stock_items.html'}).
			when('/user/edit/:userId', {controller:EditCtrl, templateUrl:'userdetail.html'}).
			when('/user/new', {controller:CreateCtrl, templateUrl:'userdetail.html'}).
			otherwise({redirectTo:'/'});
  });


function ListCtrl($scope,$http, User) {
   var self = this;
     $scope.menuGrid={};
     $scope.menuConfig = {  name:'menu',entity:{id:'vv_menu',primary:'id'}, grid:$scope.menuGrid,width:'900px',height:'200px',isSingle:true,
        columnDefs: [
        { name: 'id', visible:false},
        { name: 'menu_name', displayName: 'Name',visible:true},
        { name: 'menu_url', displayName: 'URL',visible:true},
        { name: 'menu_submenu', displayName: 'Sub Menu',visible:true }, 
        { name: 'menu_imgurl', displayName: 'Image URL' ,visible:true}                
    ]};
    
    $http({
            method: 'POST',
            url: 'http://localhost:3000/vvdata',
        data:{entity:'vv_menu'} 
        }).then(function successCallback(response) {               
            $scope.menuData = response.data;
        }, function errorCallback(response) {
           
        });
         $scope.roleGrid={};
     $scope.roleConfig = {  name:'role',entity:{id:'vv_role',primary:'id'}, grid:$scope.roleGrid,width:'300px',height:'200px',isSingle:true,
        columnDefs: [
        { name: 'id', visible:false},
        { name: 'role_name', displayName: 'Name',visible:true}        
    ]};
    
    $http({
            method: 'POST',
            url: 'http://localhost:3000/vvdata',
        data:{entity:'vv_role'} 
        }).then(function successCallback(response) {               
            $scope.roleData = response.data;
        }, function errorCallback(response) {
           
        });
}

