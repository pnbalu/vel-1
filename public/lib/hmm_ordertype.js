//app.config('pvm_hmm', [])
 app.config(function($routeProvider) {
    $routeProvider.
      when('/ordertype', {controller:OrderTypeCtrl, templateUrl:'hmm/hmm_ordertype.html'}).           
      otherwise({redirectTo:'/'});
  })


function OrderTypeCtrl($scope,$http, User, $rootScope,$location,HmmService,FmService) {
   var self = this;
    $scope.orderType =[{name:'Carry Out',value:'carryout',img:'takeaway.png',url:'#/branches?type=carryout&cboss='+$location.search().cboss},
                       {name:'Delivery',value:'delivery',img:'delivery.png',url:'#/branches?type=delivery&cboss='+$location.search().cboss},
                       {name:'Dine In',value:'dinein',img:'dinein3.png',url:'#/branches?type=dinein&cboss='+$location.search().cboss},
                       {name:'Catering',value:'catering',img:'catering.png',url:'#/branches?type=catering&cboss='+$location.search().cboss}]
      
      HmmService.setData('order_type',$location.search().type);
      
      $http({       
        url: '/get_branch',
        method: 'post',
        data:{filters:'com.company_id='+$location.search().cboss} 
        }).then(function successCallback(response) {               
            $rootScope.branches = response.data;
            $scope.branches = response.data;
            localStorage.setItem('branches',JSON.stringify($scope.branches));
            HmmService.setData('company',$location.search().cboss);
            FmService.setUser({});
            $rootScope.company=JSON.parse(localStorage.getItem('branches'))[0];
            
        }, function errorCallback(response) {
           
        });  
}

