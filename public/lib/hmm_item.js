angular.module('pvm_hmm', ['UserService'])
	.config(function($routeProvider) {
		$routeProvider.
			when('/stock', {controller:ListCtrl, templateUrl:'hmm/stock_items.html'}).	
            when('/food', {controller:FoodCtrl, templateUrl:'hmm/food.html'}). 
            when('/checkout', {controller:CheckoutCtrl, templateUrl:'hmm/checkout.html'}). 
            when('/branches', {controller:BranchCtrl, templateUrl:'hmm/branches.html'}). 		
			otherwise({redirectTo:'/'});
  })


function ListCtrl($scope,$http, User) {
   var self = this;
    
    $scope.stkItemGrid={};
    $scope.catGrid={};
    $scope.subcatGrid={};
    
    $scope.stkItemConfig = {  
            name:'stk_item',
            url:'/',
            entity:{id:'hmm_stk_item',primary:'id',parentid:'sub_cat'},
            grid:$scope.stkItemGrid,
            width:'800px',
            height:'300px',
            isSingle:true,
            parent:{id:'id',grid:$scope.subcatGrid},
        columnDefs: [
        { name: 'id', visible:false},
        { name: 'name', displayName: 'Name',visible:true,width:'40%'},
        { name: 'description', displayName: 'Description',visible:true,width:'40%'},
      //  { name: 'sub_cat', displayName: 'Sub Categeory',visible:true }, 
        { name: 'price', displayName: 'Price' ,visible:true,width:'15%'} ,
         { name: 'contents', displayName: 'Ingradients' ,visible:true,} ,
        { name: 'isAvailable', displayName: 'Available',type:'boolean' ,visible:true,width:'15%',cellTemplate: '<input type="checkbox" ng-model="row.entity.isAvailable">'}, 
        { name: 'image', displayName: 'Image ' ,visible:true,width:'30%'} ,              
    ]};
    
    $http({
            method: 'POST',
            url: '/vvdata',
        data:{entity:'hmm_stk_item'} 
        }).then(function successCallback(response) {               
            $scope.stkItemData = response.data;
        }, function errorCallback(response) {
           
        });
    

    $scope.subcatConfig = { 
         name:'subcat',
         url:'/',
         entity:{id:'hmm_subcat',primary:'id',parentid:'cat_id'}, 
         grid:$scope.subcatGrid,
         width:'350px',
         height:'300px',
         isSingle:true,
         parent:{id:'id',grid:$scope.catGrid},childObj:[$scope.stkItemConfig],
        columnDefs: [
                { name: 'id', visible:false},        
                { name: 'name', displayName: 'Name',visible:true,resizable: true,width:'65%'},
                { name: 'subcat_order', displayName: 'Order',visible:true,resizable: true,width:'35%'},    
                { name: 'cat_id', visible:false}        
        ]};  
        
     
    $scope.catConfig = {  
                        name:'cat',
                        entity:{id:'hmm_cat',primary:'id',childid:'cat_id'}, 
                        grid:$scope.catGrid,width:'300px',
                        height:'300px',
                        isSingle:true,
                        childObj:[$scope.subcatConfig],
                        columnDefs: [
                                { name: 'id', visible:false},
                                { name: 'name', displayName: 'Name',visible:true,resizable: true,width:'65%'},
                                { name: 'cat_order', displayName: 'Order',visible:true,width:'35%'}         
                        ]};
    
    $http({
        method: 'POST',
        url: '/vvdata',
        data:{entity:'hmm_cat'} 
        }).then(function successCallback(response) {               
            $scope.catData = response.data;
        }, function errorCallback(response) {
           
    });

      
    
        
}

function FoodCtrl($scope,$http, User,$rootScope,OrderService,HmmService,$window) {
    $rootScope.is_mobile = ($window.innerWidth < 480);
    $scope.isFood=true;
    $rootScope.branch=HmmService.getData('branch');
    $rootScope.orderType=HmmService.getData('order_type');
   var self = this; 
   var orderId=0;  
   $rootScope.path1=true;
   $rootScope.food=true;
    $scope.menuItem=$rootScope.pMenu   
    $scope.displaysubmenu=function(menu){
        $scope.selectedfood=menu
    }
    $scope.showBasket=function(){
        if($scope.show_basket==true){
          $scope.show_basket=false;  
        }
        if($scope.show_basket==false ||!$scope.show_basket){
          $scope.show_basket=true;  
        }
    }
   // $rootScope.tax=100;
    var orderDetail=[]
    orderDetail.tax=$rootScope.branch.branch_tax;
    function findTotal(menu){
        var total=0;
        for (var i = menu.length - 1; i >= 0; i--) {
            total=total+parseInt(menu[i].oprice)
        }
        orderDetail.tax=parseFloat($rootScope.branch.branch_tax)*total/100;
        return total;
    }
    $scope.isKitchedClosed=(HmmService.getBranch().branch_kit_close=='YES')?true:false
    $scope.kitchenClosedReason=HmmService.getBranch().branch_kit_desc;
     $scope.addItem=function(menu){
       $rootScope.show_basket=true
       menu.isAddedInCart=true;
       menu.qty=1;
       menu.oprice=parseInt(menu.price);
       menu.orderId=orderId;
       orderId++;
       orderDetail.push(angular.copy(menu))
       $rootScope.orderdetail=orderDetail;
       //$rootScope.total=findTotal(orderDetail);
        orderDetail.total=findTotal(orderDetail);;
    }
    $scope.customize=function(item){
        item.iscolsed=!item.iscolsed;
        item.isCustomized=true;
    }
    $rootScope.duplicateOrder=function(order){
        //order.qty=order.qty+1;
       // order.oprice=order.oprice+parseInt(order.price);
        var neworder= angular.copy(order)
         orderId++;
         neworder.orderId=orderId;        
         $rootScope.orderdetail.push(neworder)
         $rootScope.total=findTotal( $rootScope.orderdetail);
    }
    var id=1000;
    $rootScope.removeOrder=function(order){
        var newmenu=[]
        for (var i = 0; i < $rootScope.orderdetail.length; i++) {
           if($rootScope.orderdetail[i].orderId != order.orderId){
                newmenu.push($rootScope.orderdetail[i])
           }
           //delete $rootScope.orderdetail.splice(1,i)
        }
         $rootScope.orderdetail=newmenu
    }
     $rootScope.getAddOn=function(addon){
       
        addon=JSON.parse(addon);
        var addonMenu=[];
        for (var i = 0; i < $rootScope.pMenu.length; i++) {
            if(addon){
                for (var j = 0; j < addon.length; j++) {
                    if($rootScope.pMenu[i].sktId==addon[j]){
                        addonMenu.push($rootScope.pMenu[i]);
                    }
                } 
            }       
        }
        return addonMenu;
     }
     $rootScope.checkOut=function(order){
       $rootScope.enablecart=false
         HmmService.setOrder(order);
        /*var checkoutOrder={}
        checkoutOrder.branch='';

        $http({
        method: 'POST',
        url: '/checkout_food',
        data:{checkout: $rootScope.orderdetail} 
        }).then(function successCallback(response) {               
           
        }, function errorCallback(response) {
           
        }); */
        location.href="#/checkout" 
    }
    
    $http({
        method: 'POST',
        url: '/vvdata',
        data:{entity:'hmm_company',filters:'company_id=1'} 
        }).then(function successCallback(response) {               
            $rootScope.company = response.data[0];            
        }, function errorCallback(response) {
           
        });
    
        
}
function CheckoutCtrl($scope,$http, User,$rootScope,$location,OrderService,FmService,HmmService) {
   //$scope.orders=[{"menu":"FOOD","menu_item":"APPETIZERS","food":"Samosas - 5 Pcs","price":"7.00","description":"Fried pastry with a savory filling of spiced potatoes and green peas. A pocket bursting with flavor.","image":"samosa.jpg","currency":"$","ingrediants":{"spice":3,"salt":1,"sour":1},"isAddedInCart":true,"qty":1,"oprice":7,"orderId":0,"$$hashKey":"object:1310"},{"menu":"FOOD","menu_item":"APPETIZERS","food":"Pakodas - 12 oz","price":"7.00","description":"Crispy fried onions or Spinach dipped in a chickpea and rice flour batter. Just like amma used to make.","image":"pakoda.jpg","currency":"$","ingrediants":{"spice":3,"salt":1,"sour":1},"isAddedInCart":true,"qty":1,"oprice":7,"orderId":1,"$$hashKey":"object:1311"},{"menu":"FOOD","menu_item":"APPETIZERS","food":"Chilli Bajji - 5 Pcs","price":"8.00","description":"Chili and potato stuffing coated with batter and fried, served hot with tomato sauce or occasionally with mint and tamarind chutney.","image":"chilliBajji.jpg","currency":"$","ingrediants":{"spice":3,"salt":1,"sour":1},"isAddedInCart":true,"qty":1,"oprice":8,"orderId":2,"$$hashKey":"object:1312"}]
    var branch=HmmService.getData('branch');
    $scope.currency = branch.branch_currency;
    $scope.orders=HmmService.getOrder();
    for (var i = 0; i < $scope.orders.length; i++) {
      $scope.orders[i].iprice=$scope.orders[i].price;
    }
    $scope.orders.tips=0;
    $scope.orders.subTotal=findTotal( $scope.orders);
    $scope.orders.tax=parseFloat(branch.branch_tax)*$scope.orders.subTotal/100; 
    $scope.orders.total=$scope.orders.subTotal+$scope.orders.tax
    
    $scope.selecttips=function(tips){
        $scope.orders.tips=tips
        $scope.order.isDiscolsed=false;
    }
    function findTotal(menu){
        var total=0;
        for (var i = menu.length - 1; i >= 0; i--) {
            total=total+parseInt(menu[i].iprice)
        }
        //orderDetail.tax=parseFloat($rootScope.branch.branch_tax)*total/100;
        return total;
    }
    $scope.customize=function(item){
        item.iscolsed=!item.iscolsed;
        item.isCustomized=true;
    }//addOnItem
    $scope.addOnItem=function(item,order){
      if(order.addonip){
        order.iprice=parseFloat(order.price)+parseFloat(item.price)
      }else{
        order.iprice=order.price
      }
    }
    function addFood(order){
      order=angular.copy(order)
       $rootScope.show_basket=true
       order.isAddedInCart=true;
       order.qty=1;
       //order.oprice=parseInt(order.price);
       if(typeof orderId === 'undefined'){
          orderId=$scope.orders.length;
          orderId++;
       }
       
       orderId++;
       order.orderId=orderId
       $scope.orders.push(order)
       
       var branch=HmmService.getData('branch');
        $scope.currency = branch.branch_currency;
        
        $scope.orders.subTotal=findTotal( $scope.orders);
        $scope.orders.tax=parseFloat(branch.branch_tax)*$scope.orders.subTotal/100; 
        $scope.orders.total=$scope.orders.subTotal+$scope.orders.tax
    }
    $scope.addorder=function(order){
       addFood(order);
    }
    
    $rootScope.deleteorder=function(order){
        var newmenu=[]
        for (var i = 0; i < $scope.orders.length; i++) {
           if($scope.orders[i].orderId != order.orderId){
                newmenu.push($scope.orders[i])
           }
           //delete $rootScope.orderdetail.splice(1,i)
        }
         $scope.orders=newmenu
    }
    $scope.finalCheckout=function(orders){
        var porders={}
        porders.email=FmService.getUser().email
        porders.type="Internet"
        porders.name=orders.name;
        porders.email=orders.email;
        porders.phone=orders.phone;
        porders.total=orders.total;
        porders.tax=orders.tax;
        porders.tips=orders.tips;
        porders.paid=1;// Needs to be revisited
        porders.status='Ordered';
        porders.bemail=HmmService.getData('branch').branch_email;
        porders.branch=JSON.parse(localStorage.getItem('branches'))[0].branch_id;
        var orderArray=[]
        for (var i = 0; i < orders.length; i++) {
            var order={};
            order.food=orders[i].food;
            order.price=orders[i].price
            order.contents=JSON.stringify(orders[i].ingrediants)
            order.description=orders[i].comments
            order.isCustomized=orders[i].isCustomized?1:0
            orderArray.push(order)
        }
        porders.orderdetail=orderArray
        $http({
            method: 'POST',
            url: '/save_order',
            data:{order:porders} 
        }).then(function successCallback(response) {               
           FmService.dialogPass('Order created successfully check your e-mail. Please call us on (999-999-9999) if you want to change or cancel the order with in 10 minutes. Click "Track Order" on left top menu to see the order status.   ');
            location.href="#/food"           
        }, function errorCallback(response) {
            FmService.dialogPass('Error PLease Call Administrator'); 
        });

    }
        
}
function BranchCtrl($scope,$http, User,$rootScope,$location,HmmService,FmService) {
   //$rootScope.hmenu=true;
   $scope.orderType =[{name:'Carry Out',value:'carryout',img:'takeaway.jpg',url:'#/branches?type=carryout&cboss=1'},
                       {name:'Delivery',value:'delivery',img:'delivery.jpg',url:'#/branches?type=delivery&cboss=1'},
                       {name:'Dine In',value:'dinein',img:'dinein3.jpg',url:'#/branches?type=dinein&cboss=1'},
                       {name:'Catering',value:'catering',img:'catering.jpg',url:'#/branches?type=catering&cboss=1'}]
    $scope.selectedtype=$location.search().type
     HmmService.setData('order_type',$location.search().type);
     $http({       
        url: '/get_branch',
        method: 'post',
        data:{filters:'com.company_id='+$location.search().cboss} 
        }).then(function successCallback(response) {               
            $rootScope.branches = changeTime(response.data);
            $scope.branches = response.data;
            localStorage.setItem('branches',JSON.stringify($scope.branches));
            HmmService.setData('company',$location.search().cboss);
            FmService.setUser({});
            $rootScope.company=JSON.parse(localStorage.getItem('branches'))[0];
        }, function errorCallback(response) {
           
        });  
        function changeTime(branch){
            for (var i = 0; i < branch.length; i++) {
                branch[i].branch_time=JSON.parse(branch[i].branch_time);
            }
            return  branch;   
        }
    $scope.orderFood=function(branch){
        HmmService.setData('branch',branch);
        location.href="#/food"       
    }    
    $scope.parseme=function(times){
            return JSON.parse(times);
    }
        
}

