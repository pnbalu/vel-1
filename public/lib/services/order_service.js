app.factory('OrderService',[ '$modal', '$filter', '$timeout', function( $modal, $filter, $timeout) {

    var alertfactory = {};
    var order;
    alertfactory.setOrder = function(iorder) {
    	order=iorder;
    }  

    alertfactory.getOrder = function() {
        return order;
    }

    return alertfactory;
}]);