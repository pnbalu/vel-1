app.factory('HmmService',[ '$modal', '$filter', '$timeout', function( $modal, $filter, $timeout) {

    var alertfactory = {};
    var order;
      

    alertfactory.getBranchId = function() {
        return JSON.parse(sessionStorage.getItem('branch')).branch_id;
    }
    alertfactory.setBranch = function(param,branch) {
        sessionStorage.setItem(param,JSON.stringify(branch));
    }

    alertfactory.getCurrency = function() {
        return JSON.parse(sessionStorage.getItem('branch')).branch_currency;
    }
    alertfactory.getCompanyName = function() {
        return JSON.parse(sessionStorage.getItem('branch')).company_name;
    }
    alertfactory.getBranch = function() {
        return JSON.parse(sessionStorage.getItem('branch'));
    }
    alertfactory.setData = function(param,company) {
        sessionStorage.setItem(param,JSON.stringify(company));
    }
    alertfactory.getData = function(param) {
        return JSON.parse(sessionStorage.getItem((param)));
    }
    alertfactory.getLocData = function(param) {
        return JSON.parse(localStorage.getItem((param)));
    }
	alertfactory.getOrderStatus = function() {
    	return [{id:'Ordered',text:'Ordered'},{id:'InProgress',text:'InProgress'},{id:'Hold',text:'Hold'},{id:'Cancelled',text:'Cancelled'},{id:'Ready',text:'Ready'},{id:'Delivered',text:'Delivered'}]
	}
    alertfactory.setOrder = function(items) {
        sessionStorage.setItem('o-item',JSON.stringify(items));
    } 
    alertfactory.getOrder = function() {
        return JSON.parse(sessionStorage.getItem('o-item'));
    }
    return alertfactory;
}]);