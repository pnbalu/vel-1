app.factory('FmService',[ '$modal', '$filter', '$timeout','ngDialog', function( $modal, $filter, $timeout,ngDialog) {

    var alertfactory = {};
    var order;
    var rootscope
    alertfactory.setBranchId = function(branch) {
        localStorage.setItem('branches',branch);
    }
    alertfactory.setUser = function(user) {
        sessionStorage.setItem('user',JSON.stringify(user));
    }
    alertfactory.getRole = function(user) {
        if(sessionStorage.getItem('user')){
             return JSON.parse(sessionStorage.getItem('user')).role
        }
       
    }
    alertfactory.getUser = function(user) {
        return JSON.parse(sessionStorage.getItem('user'))
    }
    alertfactory.setRootScope = function(scope) {
       rootscope=scope
    }
    alertfactory.getRootScope = function(user) {
        return rootscope;
    }
    alertfactory.dialogPass = function(message) {
        var nestedConfirmDialog = ngDialog.openConfirm({
            template:'<div class="bg-green">'+
            '  <div class="modal-header">'+
            '    <h4 class="modal-title">Success </h4>'+
            '  </div>'+
            '  <div class="modal-body">'+
            '    <p><b>'+message+'</b></p>'+
            '  </div>'+             
            '</div>',
            plain: true
        });
    }
     alertfactory.dialogFail = function(message) {
        var nestedConfirmDialog = ngDialog.openConfirm({
            template:'<div class="bg-red">'+
            '  <div class="modal-header">'+
            '    <h4 class="modal-title">Failed </h4>'+
            '  </div>'+
            '  <div class="modal-body">'+
            '    <p><b>'+message+'</b></p>'+
            '  </div>'+             
            '</div>',
            plain: true
        });
    }

	
    return alertfactory;
}]);