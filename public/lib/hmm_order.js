angular.module('pvm_hmm_order', ['UserService'])
	.config(function($routeProvider) {
		$routeProvider.
			when('/order', {controller:OrderCtrl, templateUrl:'hmm/hmm_order.html'}).
            when('/inorder', {controller:InOrderCtrl, templateUrl:'hmm/hmm_inorder.html'}).	          
			otherwise({redirectTo:'/'});
  })




function OrderCtrl($scope,$http, User,HmmService,ngDialog) {

    function getFood(order){
    $http({
        method: 'POST',
        url: '/inorder_food',
        data:{search:order} 
        }).then(function successCallback(response) {               
           HmmService.setData('food',response.data);
           $scope.foods=response.data;
        }, function errorCallback(response) {
           
    });
    }
    $scope.upsaveOrder=function(orderdtl){
       
        
        var orders=[];
        for (var i = 0; i < orderdtl.length; i++) { 
            if(orderdtl[i].orderd_type=='NEW'){
                orders.push(orderdtl[i])
            }
        }
        for (var i = 0; i < $scope.deletedOrder.length; i++) { 
            $scope.deletedOrder[i].orderd_type='DEL'
            orders.push($scope.deletedOrder[i])           
        }
         console.log(orders)

         $scope.user=HmmService.getData('user');
        $scope.role=$scope.user.role;
        $scope.company=HmmService.getData('branch');
        
        orders.total=orders.total;
        orders.tax=orders.tax;
        orders.tips=orders.tips?orders.tips:0;
      
        orders.status='Ordered';
        orders.branch=HmmService.getBranchId();
        
        orders.bemail=HmmService.getData('branch').branch_email;
        $http({
            method: 'POST',
            url: '/save_order',
            data:{order:orders} 
        }).then(function successCallback(response) {               
              FmService.dialogPass('Food has been ordered successfully ');           
        }, function errorCallback(response) {
           
        });
    }
    $scope.addFood=function(food){
        var branch=HmmService.getData('branch');
           $scope.currency = branch.branch_currency;
        var nfood=angular.copy(food.originalObject);
        if(typeof count ==='undefined'){
            count=$scope.orderdetails.length;
        }
        nfood.id=count++;
        $scope.orderdetails.push(nfood);
        $scope.orderdetails.subTotal=findTotal( $scope.orderdetails);
         $scope.orderdetails.tax=parseFloat(branch.branch_tax)*$scope.orderdetails.total/100; 
        $scope.orderdetails.total=$scope.orderdetails.subTotal+$scope.orderdetails.tax

    }
    $scope.addInFood=function(food){
        var nfood=angular.copy(food.originalObject);
        if(typeof count ==='undefined'){
            count=$scope.orderdetails.length;
        }
     
        var branch=HmmService.getData('branch');
           $scope.currency = branch.branch_currency;
        var newOrder={orderd_contents:nfood.ingrediants,
                        orderd_customized:1,
                        orderd_description:'',
                        orderd_id:$scope.orderdetails[0].order_id,
                        orderd_name:nfood.name,
                        orderd_price:nfood.price,
                        orderd_type:'NEW'}
        nfood.id=count++;
        $scope.orderdetails.push(newOrder);
        $scope.orderdetails.total=findTotal( $scope.orderdetails);
        $scope.orderdetails.tax=parseFloat(branch.branch_tax)*$scope.orderdetails.total/100; 
        $scope.orderdetails.gtotal=$scope.orderdetails.total+$scope.orderdetails.tax

    }
    function findTotal(menu){
        var total=0;
            for (var i = 0; i < menu.length; i++) {               
            total=total+parseInt(menu[i].orderd_price)
            }            
            return total;
    }
    $scope.removeInFood=function(order){
        var branch=HmmService.getData('branch');
        $scope.currency = branch.branch_currency;
        var newmenu=[]
        var delMenu=[]
        for (var i = 0; i < $scope.orderdetails.length; i++) {
           if($scope.orderdetails[i].orderd_id != order.orderd_id){
                newmenu.push($scope.orderdetails[i])
           }else{
                delMenu.push($scope.orderdetails[i])     
           }
           //delete $rootScope.orderdetail.splice(1,i)
        }
        $scope.deletedOrder=delMenu;
        $scope.orderdetails=newmenu
        $scope.orderdetails.total=findTotal( $scope.orderdetails);
        $scope.orderdetails.tax=parseFloat(branch.branch_tax)*$scope.orderdetails.total/100; 
         $scope.orderdetails.gtotal=$scope.orderdetails.total+$scope.orderdetails.tax

    }
     var order={branch:HmmService.getBranchId()}
     getFood(order);


    $scope.user=HmmService.getData('user');
    $scope.role=$scope.user.role;
    $scope.company=HmmService.getData('branch');
    $scope.orderStatus=HmmService.getOrderStatus()
    $scope.companyName=HmmService.getCompanyName()

   var order={branch_id:HmmService.getBranchId()}
  // order.orderDate=new Date();
    getOrder(order);
    function getOrder(order){
        $http({
            method: 'POST',
            url: '/get_order',
            data:{order:order} 
            }).then(function successCallback(response) {               
                $scope.hmmorders = response.data;
                 makeDashBoard(response.data)
                 //getOrderDetails( $scope.hmmorders[0]);
            }, function errorCallback(response) {
               
        });
    }
    $scope.cContents=function(data){
        return JSON.parse(data)
    }
    function makeDashBoard(obj){
        var status=HmmService.getOrderStatus()
        var rdstatus={};
        for (var i = 0; i < status.length; i++) {
            rdstatus[status[i].id]=0;
        }
        for (var i = 0; i < obj.length; i++) {
            rdstatus[obj[i].order_status]++;             
        }
        $scope.dashboardStatus=rdstatus;
    }
    function getOrderDetails(order){
        function findTotal(menu){
        var total=0;
            for (var i = 0; i < menu.length; i++) {               
            total=total+parseInt(menu[i].orderd_price)
            }            
            return total;
        }
        var branch=HmmService.getData('branch');
       

        $scope.order =  order       
        if(order.order_id){
             $http({
                method: 'POST',
                url: '/get_order_details',
                data:{order:order.order_id} 
                }).then(function successCallback(response) {               
                    $scope.orderdetails = response.data;
                    $scope.currency = branch.branch_currency;
                     $scope.orderdetails.total=findTotal(response.data)
                    $scope.orderdetails.tax=parseFloat(branch.branch_tax)*$scope.orderdetails.total/100;        
                    $scope.orderdetails.gtotal=$scope.orderdetails.tax+$scope.orderdetails.total
                    ngDialog.open({
                        template: 'hmm/hmm_orderdetails_print.html',scope: $scope,appendClassName:'orderdetail-popup',width:'500px',height:'600px'
                    });

                }, function errorCallback(response) {
                   
            }); 
        }
    }
    $scope.showOrderDetails=function(order){ 
        $scope.order =  order       
        if(order.order_id){
             $http({
                method: 'POST',
                url: '/get_order_details',
                data:{order:order.order_id} 
                }).then(function successCallback(response) {               
                    $scope.orderdetails = response.data;
                    var branch=HmmService.getData('branch');
                     $scope.currency = branch.branch_currency;
                     $scope.orderdetails.total=findTotal(response.data)
                    $scope.orderdetails.tax=parseFloat(branch.branch_tax)*$scope.orderdetails.total/100;        
                    $scope.orderdetails.gtotal=$scope.orderdetails.tax+$scope.orderdetails.total
                    ngDialog.open({
                        template: 'hmm/hmm_orderdetails_edit.html',scope: $scope,appendClassName:'orderdetail-popup',width:'500px',height:'500px'
                    });

                }, function errorCallback(response) {
                   
            }); 
        }
    }
    $scope.printMe=function(){
        //jQuery('#ItemPrint').print();
        var divToPrint=document.getElementById('ItemPrint');
          var newWin=window.open('','Print-Window');
          newWin.document.open();
          newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
          newWin.document.close();
          setTimeout(function(){newWin.close();},1);
    }

    $scope.getOrderDetail=function(order){  
        getOrderDetails(order);
    } 
    $scope.updateXeditFood=function(order,param){
        order.bemail=HmmService.getData('branch').branch_email;
        $http({
        method: 'POST',
        url: '/update_order',
        data:{order:order,param:param} 
        }).then(function successCallback(response) {               
          
        }, function errorCallback(response) {
           
        }); 
    }
    $scope.cancelItem=function(order,param){
        var search={};       
        search.branch_id=HmmService.getBranchId();
        search.orderd_id=order.orderd_id;
        search.orderd_status='Cancelled'
        $http({
        method: 'POST',
        url: '/update_order_dtl',
        data:{order:search,param:param} 
        }).then(function successCallback(response) {               
          console.log();
        }, function errorCallback(response) {
           
        }); 
    }
    $scope.searchOrder=function(search){
        search.branch_id=HmmService.getBranchId();
         getOrder(search);
    }

}

function InOrderCtrl($scope,$http, User,HmmService,FmService) {
        $scope.user=HmmService.getData('user');
    $scope.role=$scope.user.role;
    $scope.company=HmmService.getData('branch');
    $scope.currency=HmmService.getCurrency();
    var order={branch:HmmService.getBranchId()}
    getOrder(order);
    function getOrder(order){
        $http({
            method: 'POST',
            url: '/inorder_food',
            data:{search:order} 
            }).then(function successCallback(response) {               
               HmmService.setData('food',response.data);
               $scope.foods=response.data;
            }, function errorCallback(response) {
               
        });
    }
    $scope.cContents=function(data){
        return JSON.parse(data)
    }    
    $scope.updateXeditFood=function(order,param){
        order.bemail=HmmService.getData('branch').branch_email;
        $http({
        method: 'POST',
        url: '/update_order',
        data:{order:order,param:param} 
        }).then(function successCallback(response) {               
          
        }, function errorCallback(response) {
           
        }); 
    }
    $scope.cancelItem=function(order,param){
        var search={};       
        search.branch_id=HmmService.getBranchId();
        search.orderd_id=order.orderd_id;
        search.orderd_status='Cancelled'
        $http({
        method: 'POST',
        url: '/update_order_dtl',
        data:{order:search,param:param} 
        }).then(function successCallback(response) {               
          console.log();
        }, function errorCallback(response) {
           
        }); 
    }
    $scope.admorder=[];
    var count=0;
    function findTotal(menu){
        var total=0;
        for (var i = 0; i < menu.length; i++) {
           total=total+parseFloat(menu[i].price)
        }
        return total;
    }
    $scope.addFood=function(food){
        var nfood=angular.copy(food.originalObject)
        if(!count){
            count=0;
        }
        nfood.id=count++;
        $scope.admorder.push(nfood);
        $scope.admorder.subTotal=findTotal( $scope.admorder);
        $scope.admorder.tax=100;//HmmService.getData('tax')
        $scope.admorder.total=$scope.admorder.subTotal+$scope.admorder.tax

    }

     $scope.removeFood=function(order){
        var newmenu=[]
        for (var i = 0; i < $scope.admorder.length; i++) {
           if($scope.admorder[i].id != order.id){
                newmenu.push($scope.admorder[i])
           }
           //delete $rootScope.orderdetail.splice(1,i)
        }
         $scope.admorder=newmenu
        $scope.admorder.subTotal=findTotal( $scope.admorder);
        $scope.admorder.tax=100;//HmmService.getData('tax')
        $scope.admorder.total=$scope.admorder.subTotal+$scope.admorder.tax

    }
     $scope.newOrder=function(food){
        $scope.admorder=[];
    }
     $scope.cContents=function(data){
        return JSON.parse(data)
    }
    $scope.customize=function(order){
        order.isCustomized=true;
    }
    $scope.checkout=function(orders){
        $scope.user=HmmService.getData('user');
        $scope.role=$scope.user.role;
        $scope.company=HmmService.getData('branch');
        var porders={}
        
        porders.type="To Go"
        porders.name=FmService.getUser().name;
        porders.email=FmService.getUser().email;uuupdate
        porders.bemail=$scope.company.branch_email;
        porders.phone=orders.phone
        porders.total=orders.total;
        porders.tax=orders.tax;
        porders.tips=orders.tips?orders.tips:0;
      
        porders.status='Ordered';
        porders.branch=HmmService.getBranchId();

        var orderArray=[]
        for (var i = 0; i < orders.length; i++) {
            var order={};
            order.name=orders[i].food;
            order.price=orders[i].price

            order.contents=order.contents?JSON.stringify(orders[i].ingrediants):''
            order.description=order.description?orders[i].comments:''
            order.isCustomized=orders[i].isCustomized?1:0
            orderArray.push(order)
        }
        porders.orderdetail=orderArray
        $http({
            method: 'POST',
            url: '/save_order',
            data:{order:porders} 
        }).then(function successCallback(response) {               
              FmService.dialogPass('Food has been ordered successfully ');           
        }, function errorCallback(response) {
           
        });

    }

}


