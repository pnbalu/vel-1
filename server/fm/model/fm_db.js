var path = require('path'),
    mysql = require('mysql'), 
    fmdml = require('./fm/model/fm_createdb.js');
	libUserDB = require('./fm/model/fm_userdb.js');
	utilDB = require('./fm/model/fm_utildb.js');

//This config will be auto-swapped by CF w/ proper conf. (PS: auto-reconfiguration)
var connection = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'root'
});

connection.connect(function (err) {
    if (err) return console.log(err);
    lib.setupDBAndTable(connection);
    fmdml.setupFMTable(connection)
    libUserDB.setupDBAndTable(connection);
	utilDB.setupDBAndTable(connection);
});

exports connection =