var pool;

exports.setupDBAndTable = function (pools) {
    console.log("Util DB Connected"+pools)
   pool=pools;     
};


exports.addUser = function (task, callback) {
    pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
        connection.query("INSERT INTO users (name, password, email,phonenumber) VALUES (?, ?, ?,?)", [task.name, task.password, task.email,task.phonenumber], 
    	 callback);
        
    	console.log(task)
    })
};
exports.chgPassword = function (task, callback) {
   // console.log("=====================>"+task);
    pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
        connection.query("update users set password =? where id=?", [task.password1, task.id], 
         callback);
        console.log(task)
    })
};
exports.fgPassword = function (task, callback) {
   // console.log("=====================>"+task);
    pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
        connection.query("select  password,name,email from users where email=?", [ task.email], 
         callback);
        console.log(task)
    })
};

exports.updateUser = function (id, task, callback) {
    
    var sql = "UPDATE users SET name='" + task.name
        + "', password='" + task.password
        + "', email='" + task.email
		+ "', phonenumber='" + task.phonenumber
        + "' WHERE id=" + id;
console.log(sql)
    pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
    
            connection.query(sql, callback);
        })
};

exports.getUsers = function (callback) {
     console.log("Util DB Connected"+pool)
    pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
    
         connection.query("SELECT * FROM users", callback);
        })
};

exports.getData = function (entity,callback) {
    pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
    
        connection.query("SELECT * FROM "+entity, callback);
    })
};

exports.getUser = function (id,password, callback) {
	var sql="SELECT * FROM users WHERE email='" +id+ "' and password='" +password+ "'";
	console.log('============='+sql);
    pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
    
        connection.query(sql, callback);
    })
};

exports.getFmMenu = function (role, callback) {
    
    var sql="SELECT menu_name as menu ,menu_url,menu_submenu as menu_item,menu_imgurl FROM vv_menu WHERE menu_role in ('"+role+"')";
    console.log('**************'+sql);
    pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
    
        connection.query(sql, callback);
    })
};

exports.deleteUser = function (id, callback) {
    pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
    
        connection.query("DELETE FROM users WHERE id=" + id, callback);
    })
};