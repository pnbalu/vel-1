var pool;

exports.setupDBAndTable = function (pools) {
    console.log("Util DB Connected"+pools)
   pool=pools;     
};



exports.getOrder = function (obj,callback) {
    var sql='';
    console.log(obj)
   var search=obj
   var sql='',sqlwhere='';
        console.log(search)
   if(search.orderType){
        sqlwhere= " AND order_type like '%"+search.orderType+"%' " 
   } 
   if(search.orderPhone){
        sqlwhere= sqlwhere +" AND order_phone like '%"+search.orderPhone+"%' "
   } 
   if(search.orderStatus){
        sqlwhere= sqlwhere +" AND order_status like '%"+search.orderStatus+"%' "
   } 
   if(search.orderDate){
        sqlwhere= sqlwhere +" AND DATE(order_date) = '"+search.orderDate.substring(0,10)+"' "
   }
   
   if(Object.keys(search).length>1){
         sql=' SELECT order_id, order_user,order_paid,order_email,order_price,order_status,order_tips,order_type,order_phone,order_date from hmm_order where order_branch_id='+search.branch_id+'  '+sqlwhere;
   }else{
        sql=' SELECT order_id, order_user,order_paid,order_email,order_price,order_status,order_tips,order_type,order_phone,order_date from hmm_order where order_branch_id='+search.branch_id;
   } 
    console.log(sql)
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
     connection.query( sql, callback);
   });
};
exports.getOrderDetail = function (order,callback) {
    var sql='';
    sql=' SELECT orderd_id,orderd_order_id,orderd_price,orderd_contents,orderd_name,orderd_customized,orderd_description from hmm_order_details where orderd_order_id='+order+' order by orderd_name';
   
    //console.log(sql)
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
     connection.query( sql, callback);
   });
};
exports.saveOrder = function (req,callback) {
    var b=req;
    console.log(req)
    var sql ="insert into hmm_order (order_user,order_price,order_status,order_branch_id,order_tips,order_type,order_phone,order_paid,order_email) "+
             " values ('"+b.name+"',"+b.total+",'"+b.status+"',"+b.branch+","+b.tips+",'"+b.type+"','"+b.phone+"',"+b.paid+",'"+b.email+"')";
             console.log(sql)
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
    var data=connection.query(sql, callback);   
  })
}
exports.saveOrderDetails = function (req,id,callback) {
    var b=req.orderdetail;
    console.log(req)
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
    for (var i = 0; i < b.length; i++) {
        var sql ="insert into hmm_order_details (orderd_order_id,orderd_price,orderd_contents,orderd_name,orderd_description,orderd_customized) "+
             " values ("+id+","+b[i].price+",'"+b[i].contents+"','"+b[i].food+"','"+b[i].description+"',"+b[i].isCustomized+")";
        console.log(sql)
        var data=connection.query(sql, callback);   
    }
  });
   
}
exports.updateOrder = function (order,param,callback) {
    var sql
   
        sql ='update hmm_order  set '+param+' = "'+order[param]+'" where order_id='+order.order_id;
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
   connection.query(sql, callback);
   console.log(sql)
 });
   
}
exports.graphOrder = function (order,param,callback) {
    var sql
  console.log(order)
        sql ="SELECT SUM(`order_price`) as price,SUM(`order_tips`) as tips ,order_branch_id as branch ,"+order.search.period+"(`order_date`) as period ,order_date"+
              " FROM `hmm_order` where order_branch_id='"+order.search.branch+"'  AND DATE(order_date) >= '"+order.search.from.substring(0,10)+"' AND DATE(order_date) <= '"+order.search.to.substring(0,10)+"'"+
              " GROUP BY "+order.search.period+"(`order_date`),order_branch_id order by order_branch_id,"+order.search.period+"(`order_date`)";
       
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
   connection.query(sql, callback);
   console.log(sql)
 });
   
}
exports.updateOrderDtl = function (order,param,callback) {
    var sql
    console.log(order)
        sql ='update hmm_order_details  set '+param+' = "'+order[param]+'" where orderd_id='+order.orderd_id;
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
   connection.query(sql, callback);
 });
   console.log(sql)
   
}