var pool;

exports.setupDBAndTable = function (pools) {
    console.log("Util DB Connected"+pools)
   pool=pools;     
};





exports.geItemData = function (branch,callback) {
  console.log(branch)
    var sql='';
    sql=' SELECT stk.id AS sktId ,c.name AS menu, sc.name AS menu_item,stk.name AS food,stk.price,stk.description,stk.image,branch.branch_currency AS currency,stk.contents AS ingrediants ,stk.isAvailable as available ,stk.addon as addon '+
        ' FROM hmm_cat c, hmm_subcat sc, hmm_stk_item stk,hmm_branch branch'+
        ' WHERE c.id=sc.cat_id AND sc.id=stk.sub_cat AND branch.branch_id=stk.branch_id  AND branch.branch_id='+branch+
        ' ORDER BY c.cat_order ,sc.subcat_order';
   
    console.log(sql)
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
    connection.query( sql, callback);
  });
 //console.log(pool._freeConnections.indexOf(connection));
};
 exports.getBranch = function (branch,callback) {
   var sql='';
    sql=' SELECT company_name,company_slogan,company_url,company_image,company_theme,company_logo,bra.branch_tax,bra.branch_fb,bra.branch_email,bra.branch_id,bra.branch_message,bra.branch_kit_close,branch_is_closed,bra.branch_time,bra.branch_kit_desc,bra.branch_name,bra.branch_address1,bra.branch_address2,bra.branch_city,bra.branch_state,bra.branch_zip,bra.branch_is_active,bra.branch_start_time,bra.branch_end_time,bra.branch_phone,bra.branch_map,bra.branch_currency,bra.branch_longitude,bra.branch_latitude'+
        ' FROM hmm_company com,hmm_branch bra'+
        ' WHERE com.company_id=bra.company_id and '+branch;
   
    console.log(sql)
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
        connection.query( sql, callback);
    });
};
exports.getFood = function (obj,callback) {
   var search=obj.search
   var sql='',sqlwhere='';
        console.log(search)
   if(search.main){
        sqlwhere= " AND cat.name like '%"+search.main+"%' " 
   } 
   if(search.sub){
        sqlwhere= sqlwhere +" AND scat.name like '%"+search.sub+"%' "
   } 
   if(search.food){
        sqlwhere= sqlwhere +" AND item.name like '%"+search.food+"%' "
   } 
   if(search.available){
        sqlwhere= sqlwhere +" AND item.isAvailable like '%"+search.available+"%' "
   }
   
   if(Object.keys(search).length>1){
        sql=' select item.id,cat.name as cat_name ,scat.name as cat_subname ,item.name,item.price,item.isAvailable,item.contents AS ingrediants ,item.addon as addon,item.description from hmm_cat cat ,hmm_subcat scat, hmm_stk_item item'+
        ' where cat.id=scat.cat_id and item.sub_cat=scat.id and item.branch_id='+search.branch+'  '+sqlwhere+' order by cat.id';
   }else{
        sql=' select item.id,cat.name as cat_name ,scat.name as cat_subname ,item.name,item.price,item.isAvailable,item.contents AS ingrediants,item.addon as addon,item.description from hmm_cat cat ,hmm_subcat scat, hmm_stk_item item'+
        ' where cat.id=scat.cat_id and item.sub_cat=scat.id and item.branch_id='+search.branch+' order by cat.id';
   } 
    
   
    console.log(sql)
   pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
     connection.query( sql, callback);
   })
};
exports.getAllFood = function (obj,callback) {
    console.log('================^^^^^^^^^^^^^'+obj)
    var search=obj.search  
     var  sql=' select item.id,item.name,item.price,item.contents AS ingrediants,item.isAvailable from hmm_stk_item item  where  item.branch_id='+search.branch+'  '; 
    console.log(sql)
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
        connection.query( sql, callback);
    })
};

exports.saveFood = function (food,param,callback) {
    var sql
    if(param !='price'){
        sql ="update hmm_stk_item  set  "+param+" = '"+food[param]+"' where id="+food.id;
    }else{
        sql ='update hmm_stk_item  set '+param+' = '+food[param]+' where id='+food.id;
    }
    pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
   connection.query(sql, callback);
 });
   console.log(sql)
   
}

exports.saveBranch = function (branch,param,callback) {
    var sql; 
    if(param=='branch_tax'){
      sql ='update hmm_branch  set '+param+' = '+branch[param]+' where branch_id='+branch.branch_id;
    }else{
      sql ='update hmm_branch  set '+param+' = "'+branch[param]+'" where branch_id='+branch.branch_id;
    } 
    
    
    pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
   connection.query(sql, callback);
 });
   console.log(sql)
   
}
