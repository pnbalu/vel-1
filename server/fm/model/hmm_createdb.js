var connection;
exports.setupDBAndTable = function (conn,db) {
    connection = conn;
    if (!process.env.VCAP_SERVICES) {
        connection.query('CREATE DATABASE IF NOT EXISTS  '+db+';', function (err) {
            if (err)  return console.log(err);
        });
        //Switch to 'usersDB' database
        connection.query('USE   '+db+';', function (err) {
            if (err)  return console.log(err);
        });
    }

    //setup 'users' table w/ schema
    connection.query('SHOW TABLES LIKE "hmm_stk_item";', function (err, rows) {
        if (err) return console.log(err);

        //create table if it's not already setup
        if (rows.length == 0) {
            var sql = "" +
                "CREATE TABLE hmm_stk_item(" +
                " id INT UNSIGNED NOT NULL auto_increment," +
				" name VARCHAR(50) NOT NULL default ''," +
                " main_cat VARCHAR(50) NOT NULL default ''," +
                " sub_cat VARCHAR(50) NOT NULL default ''," +
                " branch_id INT(10)  NULL default -1," +
                " price INT(10)  NULL default -1," +
				" description VARCHAR(1500) NOT NULL default ''," +
                " available_no INT(10) NOT NULL default 0," +
                " isAvailable VARCHAR(1) NOT NULL default ''," +
                " contents VARCHAR(500) NOT NULL default ''," +
                " image blob NULL ," +               
                " PRIMARY KEY (id)" +
                ");";

            connection.query(sql, function (err) {
                if (err){
                    console.log('errror while creating users table');
                    return console.log(err);   
                }
            });
        }

    });
     connection.query('SHOW TABLES LIKE "hmm_cat";', function (err, rows) {
        if (err) return console.log(err);

        //create table if it's not already setup
        if (rows.length == 0) {
            var sql = "" +
                "CREATE TABLE hmm_cat(" +
                " id INT UNSIGNED NOT NULL auto_increment," +
                " name VARCHAR(50) NOT NULL default ''," +
                " cat_order INT(3) NOT NULL default 0," +
                " PRIMARY KEY (id)" +
                ");";

            connection.query(sql, function (err) {
                if (err){
                    console.log('errror while creating users table');
                    return console.log(err);   
                }
            });
        }

    });
     connection.query('SHOW TABLES LIKE "hmm_branch";', function (err, rows) {
        if (err) return console.log(err);

        //create table if it's not already setup
        if (rows.length == 0) {
            var sql = "" +
                "CREATE TABLE hmm_branch(" +
                " branch_id INT UNSIGNED NOT NULL auto_increment," +
                " company_id INT(10) NOT NULL default 0," +
                " branch_name VARCHAR(50) NOT NULL default ''," +
                " branch_address1 VARCHAR(200) NOT NULL default ''," +
                " branch_address2 VARCHAR(200) NOT NULL default '',"+ 
                " branch_city VARCHAR(20) NOT NULL default ''," +
                " branch_state VARCHAR(20) NOT NULL default ''," +
                " branch_zip VARCHAR(10) NOT NULL default ''," +
                " branch_is_active VARCHAR(1) NOT NULL default ''," +
                " branch_start_time VARCHAR(5) NOT NULL default ''," +
                " branch_end_time VARCHAR(5) NOT NULL default '',"+ 
                " branch_is_closed VARCHAR(1) NOT NULL default ''," +
                " PRIMARY KEY (branch_id)" +
                ");";

            connection.query(sql, function (err) {
                console.log('errror while creating user_entity table');
                if (err) return console.log(err);
            });
        }

    });
   
    connection.query('SHOW TABLES LIKE "hmm_company";', function (err, rows) {
        if (err) return console.log(err);

        //create table if it's not already setup
        if (rows.length == 0) {
            var sql = "" +
                "CREATE TABLE hmm_company(" +
                " company_id INT UNSIGNED NOT NULL auto_increment," +
                " company_name VARCHAR(50) NOT NULL default ''," +
                " PRIMARY KEY (company_id)" +
                ");";

            connection.query(sql, function (err) {
                if (err){
                    console.log('errror while creating users table');
                    return console.log(err);   
                }
            });
        }

    });  
     connection.query('SHOW TABLES LIKE "hmm_subcat";', function (err, rows) {
        if (err) return console.log(err);

        //create table if it's not already setup
        if (rows.length == 0) {
            var sql = "" +
                "CREATE TABLE hmm_subcat(" +
                " id INT UNSIGNED NOT NULL auto_increment," +
                " cat_id INT(10) NOT NULL default 0," +
                " subcat_order INT(3) NOT NULL default 0," +
                " name VARCHAR(50) NOT NULL default ''," +
                " PRIMARY KEY (id)" +
                ");";

            connection.query(sql, function (err) {
                if (err){
                    console.log('errror while creating users table');
                    return console.log(err);   
                }
            });
        }

    });        

};
