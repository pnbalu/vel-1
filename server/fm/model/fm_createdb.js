var connection;
exports.setupDBAndTable = function (conn) {
    connection = conn;
    if (!process.env.VCAP_SERVICES) {
        connection.query('CREATE DATABASE IF NOT EXISTS projectsDB;', function (err) {
            if (err)  return console.log(err);
        });
        //Switch to 'usersDB' database
        connection.query('USE  projectsDB;', function (err) {
            if (err)  return console.log(err);
        });
    }

    //setup 'users' table w/ schema
    connection.query('SHOW TABLES LIKE "users";', function (err, rows) {
        if (err) return console.log(err);

        //create table if it's not already setup
        if (rows.length == 0) {
            var sql = "" +
                "CREATE TABLE users(" +
                " id INT UNSIGNED NOT NULL auto_increment," +
				" name VARCHAR(50) NOT NULL default ''," +
                " usr_ent_id INT(10)  NULL default -1," +
				" password VARCHAR(50) NOT NULL default ''," +
                " email VARCHAR(50) NOT NULL default ''," +
                " phonenumber VARCHAR(200) NOT NULL default ''," +
                " role VARCHAR(200) NOT NULL default ''," +
                " is_active VARCHAR(200) NOT NULL default ''," +
                " PRIMARY KEY (id)" +
                ");";

            connection.query(sql, function (err) {
                if (err){
                    console.log('errror while creating users table');
                    return console.log(err);   
                }
            });
        }

    });
     connection.query('SHOW TABLES LIKE "profile";', function (err, rows) {
        if (err) return console.log(err);

        //create table if it's not already setup
        if (rows.length == 0) {
            var sql = "" +
                "CREATE TABLE profile(" +
                " id INT UNSIGNED NOT NULL auto_increment," +
				" pro_type VARCHAR(50) NOT NULL default ''," +
                " pro_user_id VARCHAR(50) NOT NULL default ''," +
				" pro_user_type VARCHAR(50) NOT NULL default ''," +
                " pro_entity_id VARCHAR(50) NOT NULL default ''," +
                " pro_is_active VARCHAR(1) NOT NULL default ''," +
                " PRIMARY KEY (id)" +
                ");";

            connection.query(sql, function (err) {
                console.log('errror while creating profile table');
                if (err) return console.log(err);
            });
        }

    });
    connection.query('SHOW TABLES LIKE "user_entity";', function (err, rows) {
        if (err) return console.log(err);

        //create table if it's not already setup
        if (rows.length == 0) {
            var sql = "" +
                "CREATE TABLE user_entity(" +
                " id INT UNSIGNED NOT NULL auto_increment," +
                " usr_ent_user_id VARCHAR(50) NOT NULL default ''," +
				" usr_ent_name VARCHAR(50) NOT NULL default ''," +
                " usr_role_name VARCHAR(50) NOT NULL default ''," +
                " usr_ent_address1 VARCHAR(200) NOT NULL default ''," +
				" usr_ent_address2 VARCHAR(200) NOT NULL default '',"+ 
				" usr_ent_city VARCHAR(200) NOT NULL default ''," +
				" usr_ent_state VARCHAR(200) NOT NULL default ''," +
				" usr_ent_zip VARCHAR(200) NOT NULL default ''," +
                " usr_ent_is_active VARCHAR(1) NOT NULL default ''," +
                " PRIMARY KEY (id)" +
                ");";

            connection.query(sql, function (err) {
                console.log('errror while creating user_entity table');
                if (err) return console.log(err);
            });
        }

    });
    connection.query('SHOW TABLES LIKE "vv_role";', function (err, rows) {
        if (err) return console.log(err);

        //create table if it's not already setup
        if (rows.length == 0) {
            var sql = "" +
                "CREATE TABLE vv_role(" +
                " id INT UNSIGNED NOT NULL auto_increment," +
				" role_name VARCHAR(50) NOT NULL default ''," +
                " role_child VARCHAR(50) NOT NULL default ''," +
                " PRIMARY KEY (id)" +
                ");";

            connection.query(sql, function (err) {
                console.log('errror while creating role table');
                if (err) return console.log(err);
            });
        }

    });
     connection.query('SHOW TABLES LIKE "token";', function (err, rows) {
        if (err) return console.log(err);

        //create table if it's not already setup
        if (rows.length == 0) {
            var sql = "" +
                "CREATE TABLE token(" +
                " id INT UNSIGNED NOT NULL auto_increment," +
                " tkn_ent_id VARCHAR(50) NOT NULL default ''," +
				" tkn_count VARCHAR(50) NOT NULL default ''," +
                " tkn_start VARCHAR(50) NOT NULL default ''," +
                " tkn_name VARCHAR(200) NOT NULL default ''," +
                " tkn_date VARCHAR(200) NOT NULL default ''," +
                " PRIMARY KEY (id)" +
                ");";

            connection.query(sql, function (err) {
                console.log('errror while creating token table');
                if (err) return console.log(err);
            });
        }

    });
    connection.query('SHOW TABLES LIKE "token_details";', function (err, rows) {
        if (err) return console.log(err);

        //create table if it's not already setup
        if (rows.length == 0) {
            var sql = "" +
                "CREATE TABLE token_details(" +
                " id INT UNSIGNED NOT NULL auto_increment," +
                " tkn_id VARCHAR(50) NOT NULL default ''," +
				" tkn_name VARCHAR(50) NOT NULL default ''," +
                " tkn_user VARCHAR(50) NOT NULL default ''," +
                " tkn_done VARCHAR(1) NOT NULL default ''," +
                " PRIMARY KEY (id)" +
                ");";

            connection.query(sql, function (err) {
                console.log('errror while creating token_details table');
                if (err) return console.log(err);
            });
        }

    });
	connection.query('SHOW TABLES LIKE "vv_menu";', function (err, rows) {
        if (err) return console.log(err);

        //create table if it's not already setup
        if (rows.length == 0) {
            var sql = "" +
                "CREATE TABLE vv_menu(" +
                " id INT UNSIGNED NOT NULL auto_increment," +
				" menu_name VARCHAR(50) NOT NULL default ''," +
                " menu_url VARCHAR(500) NOT NULL default ''," +
				" menu_submenu VARCHAR(50) NOT NULL default ''," +
                " role VARCHAR(50) NOT NULL default ''," +
                " menu_imgurl VARCHAR(500) NOT NULL default ''," +
                " menu_order INT(2) NOT NULL default 0," +
                " PRIMARY KEY (id)" +
                ");";

            connection.query(sql, function (err) {
                if (err) return console.log(err);
            });
        }

    });   

};
