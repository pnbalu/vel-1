var pool;

exports.setupDBAndTable = function (pools) {
    console.log("Util DB Connected"+pools)
   pool=pools;     
};


exports.getLovData = function (entity,fields,callback) {
	var sql="";
	if(fields){
	 sql="SELECT "+fields+" FROM "+entity;
	}else{
	 sql="SELECT * FROM "+entity;
	}	
    console.log(sql)
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
    connection.query(sql, callback);
	});
};
exports.getGridData = function (entity,fields,filters,callback) {
	var sql="";
	if(fields){
		if(filters){
			sql="SELECT "+fields+" FROM "+entity+" where "+filters;
		}else{
			sql="SELECT "+fields+" FROM "+entity;
		}	 
	}else{
	 sql="SELECT * FROM "+entity;
	}	
    console.log(sql)
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
    connection.query(sql, callback);
		});
};
exports.deleteVVData = function (entity,filters,callback) {
	var sql="";	
	sql="DELETE from "+entity+" where "+filters;		
    console.log(sql)
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
    connection.query(sql, callback);
		});
};
exports.saveGridData = function (req,callback) {
	 pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
	var b=req.data;
	var c=req.config;
	for (var i=0;i < b.length;i++) {
		console.log(b[i].opp)
		if(b[i].opp=='N'){
			var fieldlist=''
			var values='';
			
			for (field in b[i]) {
				if(field!='opp' && field!='id'){
					fieldlist=field+","+fieldlist;
					values="'"+b[i][field]+"',"+values
				}
			}
			var sql ="insert into "+c.entity+' ( '+fieldlist.substring(1,fieldlist.length-1)+ ') values ('+values.substring(3,values.length-1)+')';
			connection.query(sql, callback);
			console.log(sql)
		}
		if(b[i].opp=='E'){
			var map="";
			for (field in b[i]) {
				if(field!='opp' && field!='id'){
					map= map +" "+field+"='"+b[i][field] +"',";					
				}
			}
			var sql ="update "+c.entity+' set '+map.substring(0,map.length-1)+'where id='+b[i].id;
			connection.query(sql, callback);
			console.log(sql)
		}
	}
});
}
exports.getMenu = function (callback) {
	var sql="";
	 sql="SELECT * FROM vv_menu order by menu_order asc";		
    console.log(sql)
     pool.getConnection(function (err,connection) {
        if (err) {
            connection.release();
            console.log(err)
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }
    connection.query(sql, callback);
})
};
    
