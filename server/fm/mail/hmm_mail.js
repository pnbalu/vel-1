var transport;

exports.init = function (tr) {
    console.log("Util DB Connected")
   transport=tr;     
};

exports.sendFoodConfirmation = function (opt) {
    var mailOptions={
        to : opt.to,
        from:opt.from,
        subject : opt.subject,
        html : ' Hi,<br> We have received your Order ('+opt.orderid+'), Contact you shortly<br>Regards<br> Admin'
    }
    console.log(mailOptions);
    transport.sendMail(mailOptions, function(error, response){
     if(error){
            console.log(error);
       // response.end("error");
     }else{
            console.log("Message sent: " + response.message);
       // response.end("sent");
         }
	});    
};
exports.sendEMail = function (opt) {
    var mailOptions={
        to : opt.to,
        from:opt.from,
        subject : opt.subject,
        html : ' <b>Dear '+opt.name+',<br> '+opt.text+'<br>Regards <br> Admin'
    }
    console.log(mailOptions);
    transport.sendMail(mailOptions, function(error, response){
     if(error){
            console.log(error);
       // response.end("error");
     }else{
            console.log("Message sent: " + response.message);
       // response.end("sent");
         }
    });    
};
exports.sendMail = function (opt) {
    var mailOptions={
        to : opt.to,
        from:opt.from,
        subject : opt.subject,
        html : ' <b>Dear '+opt.name+', <br> Your requested new password is '+opt.password+'<br> If the password is not working feel free to contact the number which is in the website <br> Regards <br> Admin'
    }
    console.log(mailOptions);
    transport.sendMail(mailOptions, function(error, response){
     if(error){
            console.log(error);
       // response.end("error");
     }else{
            console.log("Message sent: " + response.message);
       // response.end("sent");
         }
    });    
};

exports.sendConfMail = function (opt) {
    var mailOptions={
        to : opt.to,
        from:opt.from,
        subject : opt.subject,
        html : ' <b>Dear '+opt.name+', <br> Please confirm the email link below. Once it is activated you can login to the website <br> http://raptorzeil.com/verified?email='+opt.email+'<br>Regards<br>Admin'
    }
    console.log(mailOptions);
    transport.sendMail(mailOptions, function(error, response){
     if(error){
            console.log(error);
       // response.end("error");
     }else{
            console.log("Message sent: " + response.message);
       // response.end("sent");
         }
    });    
};



