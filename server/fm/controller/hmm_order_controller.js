function init(app) {
	console.log('Order controller init')

	app.post('/save_order', function (req, res) {
		var orderid=0;		
		hmmOrderDB.saveOrder(req.body.order, function (err, rows) {
			//console.log(req.body.order)
			orderid=rows.insertId;
			hmmOrderDB.saveOrderDetails(req.body.order,rows.insertId, function (err, rows) {
				//console.log(rows)
				//console.log(req.body.order)	
				return err ? res.json(err) : res.json(rows );
			});	
			hmm_mail.sendFoodConfirmation({orderid:orderid,name:req.body.order.name,to:req.body.order.email,from:req.body.order.bemail,subject:'Your Order Confirmed'})
			return err ? res.json(err) : res.json(rows );
		});
		
		
	});	
	app.post('/get_order', function (req, res) {		
		hmmOrderDB.getOrder(req.body.order, function (err, rows) {
			//console.log(rows)			
			return err ? res.json(err) : res.json(rows );
		});
		
	});	
	app.post('/get_order_details', function (req, res) {		
		hmmOrderDB.getOrderDetail(req.body.order, function (err, rows) {
			//console.log(rows)			
			return err ? res.json(err) : res.json(rows );
		});
		
	});
	app.post('/update_order', function (req, res) {		
		hmmOrderDB.updateOrder(req.body.order,req.body.param, function (err, rows) {
			req.session.isLoggedIn=true;
			console.log(rows)
			console.log(req.body.order)	
			if(req.body.order.order_status=='Ready' ||req.body.order.order_status=='Cancelled' ||req.body.order.order_status=='Hold' ){
				hmm_mail.sendEMail({name:req.body.order.order_user,to:req.body.order.order_email,from:req.body.order.bemail,subject:'Your Order is '+req.body.order.order_status,text:'Your Order is '+req.body.order.order_status})	
			}
				
				
			return err ? res.json(err) : res.json(rows );
		});		
	});	
	app.post('/graph_order', function (req, res) {		
		hmmOrderDB.graphOrder(req.body.order,req.body.param, function (err, rows) {
			req.session.isLoggedIn=true;		
			return err ? res.json(err) : res.json(rows );
		});		
	});	
	app.post('/update_order_dtl', function (req, res) {		
		hmmOrderDB.updateOrderDtl(req.body.order,req.body.param, function (err, rows) {
			req.session.isLoggedIn=true;		
			return err ? res.json(err) : res.json(rows );
		});		
	});
}
module.exports.init = init;