function init(app) {
	console.log('User controller init')
	app.post('/saveuser', function (req, res) {
		var b = req.body.user;
		var user = {name:b.name, email:b.email, phonenumber:b.phonenumber,password:b.password};
		
		libUserDB.addUser(user, function (err, info) {
			if (err) {
				return res.json({"error":"something went wrong" + err});
			}
			console.log('------------p----------------------->'+b);
			var message={to:b.email,from:b.cemail,subject:'Email Verification',name:b.name,email:b.email}
			hmm_mail.sendConfMail(message)
			res.json(info[0]);			
		});
	});
	app.post('/chgPassword', function (req, res) {
		var b = req.body.user;
		libUserDB.chgPassword(b, function (err, info) {
			if (err) {
				return res.json({"error":" Error in Change Password" + err});
			}
			b.id = info.insertId;
			res.json(b);
		});
	});
	app.post('/fgPassword', function (req, res) {
		var b = req.body.user;
		libUserDB.fgPassword(b, function (err, info) {
			if (err) {
				return res.json({"error":" Error in Change Password" + err});
			}

			var message={to:b.email,from:b.cemail,subject:'Your Password',name:info[0].name,password:info[0].password}
			console.log('------------p----------------------->'+JSON.stringify(message));
			//b.id = info.insertId;
			res.json(info[0]);
			hmm_mail.sendMail(message)
		});
	});

	//Create
	app.post('/user', function (req, res) {
		var b = req.body;
		var user = {name:b.name, email:b.email, phonenumber:b.phonenumber,password:b.password};
	console.log('------------p----------------------->');
		libUserDB.addUser(user, function (err, info) {
			if (err) {
				return res.json({"error":"something went wrong" + err});
			}
			user.id = info.insertId;
			res.json(user);
		});
	});

	//Read
	app.get('/user', function (req, res) {
		//if id is passed, return that user
		console.log('----------------------------------->'+req.query.id);
		if (req.query.id) {
			libUserDB.getUser(req.query.id,req.query.password, function (err, rows) {
				req.session.isLoggedIn=true;
				console.log('----------------------------------->'+rows);
				return err ? res.json(err) : res.json(rows && rows[0]);
			});
		} else { //return all users
			libUserDB.getUsers(function (err, rows) {
				return err ? res.json(err) : res.json(rows);
			});
		}
	});
	app.get('/login', function (req, res) {
		//if id is passed, return that user
		console.log('------Login-------------->'+req.query);
		libUserDB.getUser(req.query.id,req.query.password, function (err, rows) {
				req.session.isLoggedIn=true;
				console.log('----------------------------------->'+rows);
				return err ? res.json(err) : res.json(rows && rows[0]);
		});
		
	});
	//Update
	app.put('/user', function (req, res) {
		var b = req.body;
		var user = {name:b.name, password:b.password, phonenumber:b.phonenumber,email:b.email,};
	console.log('-------------------------put---------->');
		libUserDB.updateUser(req.query.id, user, function (err, info) {
			if (err) {
				return res.json({"error":"something went wrong" + err});
			}
			res.json(user);
		});
	});


	//Delete
	app.delete('/user', function (req, res) {
		libUserDB.deleteUser(req.query.id, function (err, info) {
			res.json({"Error":err});
		});
	});

	app.post('/fm_menu', function (req, res) {
		var b = req.body.role;		
		libUserDB.getFmMenu(b, function (err, info) {
			if (err) {
				return res.json({"error":"something went wrong" + err});
			}
			
			res.json(info);
		});
	});
}
module.exports.init = init;