function init(app) {
	console.log('OrderType controller init')

	app.post('/getordertype', function (req, res) {		
		hmmOrderDB.getOrderData(req.body.role, function (err, rows) {
			req.session.isLoggedIn=true;		
			return err ? res.json(err) : res.json(rows );
		});
		
	});	
}
module.exports.init = init;