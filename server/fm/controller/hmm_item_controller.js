function init(app) {
	console.log('Item controller init')

	app.post('/getkmenu', function (req, res) {		
		hmmItemDB.geItemData(req.body.branch_id, function (err, rows) {
			req.session.isLoggedIn=true;		
			return err ? res.json(err) : res.json(rows );
		});
		
	});
	app.post('/get_branch', function (req, res) {		
		hmmItemDB.getBranch(req.body.filters, function (err, rows) {
			req.session.isLoggedIn=true;		
			return err ? res.json(err) : res.json(rows );
		});
		
	});
	app.post('/get_food', function (req, res) {		
		hmmItemDB.getFood(req.body, function (err, rows) {
			req.session.isLoggedIn=true;		
			return err ? res.json(err) : res.json(rows );
		});
		
	});
	app.post('/inorder_food', function (req, res) {		
		hmmItemDB.getAllFood(req.body, function (err, rows) {
			req.session.isLoggedIn=true;		
			return err ? res.json(err) : res.json(rows );
		});
		
	});
	app.post('/save_food', function (req, res) {		
		hmmItemDB.saveFood(req.body.food,req.body.param, function (err, rows) {
			req.session.isLoggedIn=true;		
			return err ? res.json(err) : res.json(rows );
		});
		
	});
	app.post('/save_branch', function (req, res) {		
		hmmItemDB.saveBranch(req.body.branch,req.body.param, function (err, rows) {
			req.session.isLoggedIn=true;		
			return err ? res.json(err) : res.json(rows );
		});
		
	});
}
module.exports.init = init;