function init(app) {
	console.log('User controller init')
	app.get('/vvData1', function (req, res) {
    //if id is passed, return that user
	utilDB.getLovData(req.body.entity,req.body.fieldlist, function (err, rows) {
	req.session.isLoggedIn=true;
	console.log('----------------------------------->'+rows);
	return err ? res.json(err) : res.json(rows );
	});
    
	});
	app.post('/vvdata', function (req, res) {
		//if id is passed, return that user
		//console.log('----------------------------------->'+JSON.stringify(req));
		utilDB.getGridData(req.body.entity,req.body.fieldlist,req.body.filters, function (err, rows) {
		req.session.isLoggedIn=true;
		console.log('----------------------------------->'+rows);
		return err ? res.json(err) : res.json(rows );
		});
		
	});
	app.post('/lov', function (req, res) {
		//if id is passed, return that user
		console.log('----------------------------------->'+JSON.stringify(req.body));
			utilDB.getLovData(req.body.entity,req.body.fieldlist, function (err, rows) {
			//req.session.isLoggedIn=true;
			console.log('----------------------------------->'+rows);
			return err ? res.json(err) : res.json(rows );
		});
		
	});	
	app.post('/vvsavel', function (req, res) {
		var b = req.body.data;
		utilDB.saveGridData(req.body, function (err, rows) {
			req.session.isLoggedIn=true;
			console.log('----------------------------------->'+rows);
			return err ? res.json(err) : res.json(rows );
		});
	});
	app.post('/vvdelete', function (req, res) {
		var b = req.body.data;
		utilDB.deleteVVData(req.body.entity,req.body.where, function (err, rows) {
			req.session.isLoggedIn=true;
			console.log('----------------------------------->'+rows);
			return err ? res.json(err) : res.json(rows );
		});
	});
	app.post('/getmenu', function (req, res) {
		var b = req.body.data;
		utilDB.getMenu(function (err, rows) {
			req.session.isLoggedIn=true;
			console.log('----------------------------------->'+rows);
			return err ? res.json(err) : res.json(rows );
		});
	});
}
module.exports.init = init;