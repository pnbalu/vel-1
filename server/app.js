var express = require('express'),
    http = require('http'),
    path = require('path'),
    mysql = require('mysql'); 
    var config = require('config');
    nodemailer = require('nodemailer');
    smtpTransport= require('nodemailer-smtp-transport');
  //  fmdml = require('./fm/model/fm_createdb.js');
  //  hmmdml = require('./fm/model/hmm_createdb.js');
	libUserDB = require('./fm/model/fm_userdb.js');
	utilDB = require('./fm/model/fm_utildb.js');
    hmmItemDB= require('./fm/model/hmm_item_db.js');
    hmmOrderDB= require('./fm/model/hmm_order_db.js');
    hmmOrderTypeDB= require('./fm/model/hmm_ordertype_db.js');

	user_controller = require('./fm/controller/user_controller.js');
	util_controller = require('./fm/controller/util_controller.js');
    hmm_item_controller = require('./fm/controller/hmm_item_controller.js');
    hmm_order_controller = require('./fm/controller/hmm_order_controller.js');
    hmm_ordertype_controller = require('./fm/controller/hmm_ordertype_controller.js');

    hmm_mail = require('./fm/mail/hmm_mail.js');

var dbConfig = config.get('db.config');
var app = express();
var db_name='projectsDB'
var store  = new express.session.MemoryStore;
app.use(express.cookieParser());
app.use(express.session({ secret: 'something', store: store }));

//This config will be auto-swapped by CF w/ proper conf. (PS: auto-reconfiguration)

var pool =mysql.createPool(dbConfig);/*
var transporter = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    host: 'smtp.gmail.com',
     port: 465,
    secure: true, 
    auth: {
        user: 'raptorziel@gmail.com', // my mail
        pass: 'Vijay1234'
    }
}))*/
var transporter = nodemailer.createTransport(config.get('email.config'));
transporter.on('log', console.log);
hmm_mail.init(transporter);


libUserDB.setupDBAndTable(pool);
utilDB.setupDBAndTable(pool);
hmmItemDB.setupDBAndTable(pool);
hmmOrderDB.setupDBAndTable(pool);
hmmOrderTypeDB.setupDBAndTable(pool);

app.configure(function () {
    app.set('port', process.env.PORT || 8080);
    app.use(express.favicon());
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser('your secret here'));
    app.use(express.session());
    app.use(app.router);
    app.use(express.static(path.join(__dirname, '../public')));
});

app.configure('development', function () {
    app.use(express.errorHandler());
});

user_controller.init(app);
util_controller.init(app);
hmm_item_controller.init(app);
hmm_order_controller.init(app);
hmm_ordertype_controller.init(app);


http.createServer(app).listen(app.get('port'), function () {
    console.log("Express server listening on port " + app.get('port'));
});